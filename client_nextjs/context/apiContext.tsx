import { createContext, useContext, useState, useEffect, ReactNode } from 'react';

// Typage des données des podcasts
type Podcast = {
  id: string;
  attributes: {
    date: string;
    description: string;
    duree: string;
    episode: number;
    mp3Lien: string;
    nb_ecoutes: number;
    slug: string;
    titre: string;
    categories: {
      data: {
        attributes: {
          nom: string;
        };
      }[];
    };
    img: {
      data: {
        attributes: {
          url: string;
        };
      };
    };
  };
};

// Typage du contexte
type ApiContextValue = {
  datas: Podcast[];
};

// Création du contexte API initié à un tableau vide
const ApiContext = createContext<ApiContextValue>({ datas: [] });

// Composant ApiProvider qui fournit les données du contexte aux autres composants de l'application
export default function ApiProvider({ children }: { children: ReactNode }) {

  // Stocker les données des podcasts
  const [datas, setDatas] = useState([]);

  // Requête à l'API pour récupérer les données des podcasts
  useEffect(() => {
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/podcasts/?populate=categories,img`)
      .then(response => response.json())
      .then(data => {
        setDatas(data.data);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  
  // Rendu du composant ApiProvider avec le contexte ApiContext.Provider
  return (
    <ApiContext.Provider value={{ datas }}>
      {children}
    </ApiContext.Provider>
  )
}

// Fonction personnalisée useApi pour accéder aux données du contexte depuis d'autres composants
export function useApi() {
  return useContext(ApiContext);
}