import Head from 'next/head'
import Link from "next/link";
import { useApi } from '../context/apiContext';
import Collaborate from "@/components/Collaborate";
import NewsLetter from "@/components/Newsletter";

import { BiLogoFacebook } from 'react-icons/bi';
import { BiLogoInstagram } from 'react-icons/bi';
import { BiLogoLinkedin } from 'react-icons/bi';
import { BiLogoTwitter } from 'react-icons/bi';
import { MdHeadphones } from 'react-icons/md';
import { HiOutlineArrowNarrowDown } from 'react-icons/hi';
import { BsPlayCircle } from 'react-icons/bs';
import { TbLetterM } from 'react-icons/tb';
import { RiDoubleQuotesL } from 'react-icons/ri';
import { BsPlayCircleFill } from 'react-icons/bs';



export default function Home() {

  // Récupération des podcasts de apiContext
  const { datas } = useApi();

  // Sélection des podcasts pour affichage dans la section actualités
  const lastPodcasts = datas.slice(datas.length - 5, datas.length).reverse();
  const lastPodcastsMd = datas.slice(datas.length - 6, datas.length - 1).reverse();
  const lastPodcast = datas[datas.length - 1];


  return (
    <>

      <Head>
        <title> Écoutez les Meilleurs Podcasts sur le Numérique avec Verywell.Podcast </title>
        <meta name="description" content="Découvrez une sélection exceptionnelle de podcasts passionnants et informatifs sur le monde du numérique. Plongez dans des sujets variés tels que la technologie, l'innovation, la cybersécurité et bien plus encore." />
        <meta property="og:title" content="Écoutez les Meilleurs Podcasts sur le Numérique avec Verywell.Podcast" />
        <meta property="og:description" content="Découvrez une sélection exceptionnelle de podcasts passionnants et informatifs sur le monde du numérique. Plongez dans des sujets variés tels que la technologie, l'innovation, la cybersécurité et bien plus encore." />
        <meta property="og:url" content={`${process.env.NEXT_PUBLIC_APP_URL}/`} />
        <meta property="og:type" content="website" />
        <link rel="icon" href="/img/logo.png" />
      </Head>

      <div className="bg-white">

        {/* SECTION HERO AREA */}
        <div className="hero-bg relative">
          <div className="hero-desaturate absolute w-full h-full" style={{ backgroundImage: `url(/img/accueil/hero.jpg)`, backgroundPosition: 'center', backgroundSize: 'cover' }}></div>
          <div className="absolute bottom-0 w-full h-1/5" style={{ background: 'linear-gradient(180deg, rgba(25, 28, 38, 0), rgb(25, 28, 38))' }}></div>
          <div className="hero-cover"></div>
          <section className="h-screen position relative text-white">

            {/* Titres principaux */}
            <div className="title-home absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 pb-12 text-center font-bold">
              {/* <h1 className="outline-text text-5xl">Verywell.Podcast</h1> */}

              {/* Titre 1 grand écran */}
              <svg id="logo" className="hidden sm:block" width="524" height="86" viewBox="0 0 524 86" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.6161 71L0.914062 0.530029H14.2251L21.7941 48.38L28.8411 0.530029H42.9351L30.0591 71H13.6161Z" stroke="white" strokeWidth="3" />
                <path d="M60.0292 71.87C56.3752 71.87 53.2432 71.232 50.6332 69.956C48.0232 68.622 46.0222 66.708 44.6302 64.214C43.2382 61.662 42.5422 58.617 42.5422 55.079V36.635C42.5422 33.039 43.2382 29.994 44.6302 27.5C46.0222 25.006 48.0232 23.121 50.6332 21.845C53.3012 20.511 56.4332 19.844 60.0292 19.844C63.7412 19.844 66.8152 20.482 69.2512 21.758C71.7452 23.034 73.6012 24.919 74.8192 27.413C76.0952 29.907 76.7332 32.981 76.7332 36.635V46.727H56.5492V56.384C56.5492 57.66 56.6942 58.704 56.9842 59.516C57.2742 60.328 57.6802 60.908 58.2022 61.256C58.7242 61.604 59.3332 61.778 60.0292 61.778C60.7252 61.778 61.3342 61.633 61.8562 61.343C62.4362 60.995 62.8712 60.473 63.1612 59.777C63.5092 59.081 63.6832 58.124 63.6832 56.906V51.599H76.7332V56.471C76.7332 61.575 75.2542 65.432 72.2962 68.042C69.3382 70.594 65.2492 71.87 60.0292 71.87ZM56.5492 40.115H63.6832V35.069C63.6832 33.735 63.5092 32.691 63.1612 31.937C62.8712 31.183 62.4362 30.661 61.8562 30.371C61.3342 30.081 60.6962 29.936 59.9422 29.936C59.2462 29.936 58.6372 30.11 58.1152 30.458C57.6512 30.806 57.2742 31.415 56.9842 32.285C56.6942 33.097 56.5492 34.286 56.5492 35.852V40.115Z" stroke="white" strokeWidth="3" />
                <path d="M79.4064 71V20.714H93.5874V29.066C95.2114 26.166 96.8354 23.962 98.4594 22.454C100.083 20.888 102.055 20.105 104.375 20.105C104.839 20.105 105.245 20.134 105.593 20.192C105.941 20.192 106.26 20.25 106.55 20.366V34.46C105.912 34.17 105.187 33.938 104.375 33.764C103.621 33.532 102.809 33.416 101.939 33.416C100.315 33.416 98.8074 33.851 97.4154 34.721C96.0234 35.591 94.7474 36.78 93.5874 38.288V71H79.4064Z" stroke="white" strokeWidth="3" />
                <path d="M108.709 85.616V75.785C110.507 75.785 111.986 75.669 113.146 75.437C114.364 75.205 115.263 74.77 115.843 74.132C116.481 73.494 116.8 72.624 116.8 71.522C116.8 70.536 116.626 69.347 116.278 67.955C115.93 66.505 115.495 64.881 114.973 63.083L103.141 20.714H116.452L122.803 49.772L127.588 20.714H140.986L129.415 72.218C128.661 75.698 127.472 78.395 125.848 80.309C124.224 82.281 122.223 83.644 119.845 84.398C117.525 85.21 114.886 85.616 111.928 85.616H108.709Z" stroke="white" strokeWidth="3" />
                <path d="M145.879 71L138.484 20.714H150.316L153.97 49.25L157.885 20.714H168.151L171.544 49.946L175.807 20.714H186.769L179.287 71H167.107L162.757 42.725L157.885 71H145.879Z" stroke="white" strokeWidth="3" />
                <path d="M204.228 71.87C200.574 71.87 197.442 71.232 194.832 69.956C192.222 68.622 190.221 66.708 188.829 64.214C187.437 61.662 186.741 58.617 186.741 55.079V36.635C186.741 33.039 187.437 29.994 188.829 27.5C190.221 25.006 192.222 23.121 194.832 21.845C197.5 20.511 200.632 19.844 204.228 19.844C207.94 19.844 211.014 20.482 213.45 21.758C215.944 23.034 217.8 24.919 219.018 27.413C220.294 29.907 220.932 32.981 220.932 36.635V46.727H200.748V56.384C200.748 57.66 200.893 58.704 201.183 59.516C201.473 60.328 201.879 60.908 202.401 61.256C202.923 61.604 203.532 61.778 204.228 61.778C204.924 61.778 205.533 61.633 206.055 61.343C206.635 60.995 207.07 60.473 207.36 59.777C207.708 59.081 207.882 58.124 207.882 56.906V51.599H220.932V56.471C220.932 61.575 219.453 65.432 216.495 68.042C213.537 70.594 209.448 71.87 204.228 71.87ZM200.748 40.115H207.882V35.069C207.882 33.735 207.708 32.691 207.36 31.937C207.07 31.183 206.635 30.661 206.055 30.371C205.533 30.081 204.895 29.936 204.141 29.936C203.445 29.936 202.836 30.11 202.314 30.458C201.85 30.806 201.473 31.415 201.183 32.285C200.893 33.097 200.748 34.286 200.748 35.852V40.115Z" stroke="white" strokeWidth="3" />
                <path d="M224.041 71V0.530029H238.135V71H224.041Z" stroke="white" strokeWidth="3" />
                <path d="M242.695 71V0.530029H256.789V71H242.695Z" stroke="white" strokeWidth="3" />
                <path d="M260.305 71V57.602H273.703V71H260.305Z" stroke="white" strokeWidth="3" />
                <path d="M277.543 71V0.530029H301.207C305.441 0.530029 308.921 1.34203 311.647 2.96603C314.373 4.53203 316.403 6.85203 317.737 9.92603C319.071 12.942 319.738 16.625 319.738 20.975C319.738 26.427 318.839 30.603 317.041 33.503C315.243 36.345 312.807 38.317 309.733 39.419C306.659 40.521 303.208 41.072 299.38 41.072H293.116V71H277.543ZM293.116 30.197H298.336C300.25 30.197 301.7 29.849 302.686 29.153C303.672 28.457 304.31 27.413 304.6 26.021C304.948 24.629 305.122 22.86 305.122 20.714C305.122 18.916 304.977 17.35 304.687 16.016C304.455 14.624 303.846 13.522 302.86 12.71C301.874 11.898 300.337 11.492 298.249 11.492H293.116V30.197Z" stroke="white" strokeWidth="3" />
                <path d="M337.687 71.87C334.091 71.87 330.959 71.261 328.291 70.043C325.681 68.825 323.651 66.998 322.201 64.562C320.809 62.126 320.113 59.081 320.113 55.427V36.287C320.113 32.633 320.809 29.588 322.201 27.152C323.651 24.716 325.681 22.889 328.291 21.671C330.959 20.453 334.091 19.844 337.687 19.844C341.283 19.844 344.386 20.453 346.996 21.671C349.664 22.889 351.723 24.716 353.173 27.152C354.623 29.588 355.348 32.633 355.348 36.287V55.427C355.348 59.081 354.623 62.126 353.173 64.562C351.723 66.998 349.664 68.825 346.996 70.043C344.386 71.261 341.283 71.87 337.687 71.87ZM337.774 62.474C338.702 62.474 339.427 62.242 339.949 61.778C340.471 61.256 340.819 60.56 340.993 59.69C341.225 58.762 341.341 57.66 341.341 56.384V35.33C341.341 34.054 341.225 32.981 340.993 32.111C340.819 31.183 340.471 30.487 339.949 30.023C339.427 29.501 338.702 29.24 337.774 29.24C336.846 29.24 336.121 29.501 335.599 30.023C335.077 30.487 334.7 31.183 334.468 32.111C334.236 32.981 334.12 34.054 334.12 35.33V56.384C334.12 57.66 334.236 58.762 334.468 59.69C334.7 60.56 335.077 61.256 335.599 61.778C336.121 62.242 336.846 62.474 337.774 62.474Z" stroke="white" strokeWidth="3" />
                <path d="M368.868 71.87C365.156 71.87 362.285 70.565 360.255 67.955C358.225 65.345 357.21 61.488 357.21 56.384V35.591C357.21 32.459 357.645 29.733 358.515 27.413C359.385 25.035 360.69 23.179 362.43 21.845C364.17 20.511 366.316 19.844 368.868 19.844C370.724 19.844 372.493 20.221 374.175 20.975C375.857 21.671 377.394 22.628 378.786 23.846V0.530029H392.967V71H378.786V67.085C377.22 68.593 375.596 69.782 373.914 70.652C372.232 71.464 370.55 71.87 368.868 71.87ZM375.045 62.822C375.509 62.822 376.06 62.706 376.698 62.474C377.394 62.242 378.09 61.923 378.786 61.517V29.762C378.206 29.472 377.597 29.211 376.959 28.979C376.321 28.747 375.712 28.631 375.132 28.631C373.856 28.631 372.928 29.182 372.348 30.284C371.768 31.328 371.478 32.604 371.478 34.112V57.254C371.478 58.298 371.594 59.255 371.826 60.125C372.058 60.937 372.435 61.604 372.957 62.126C373.479 62.59 374.175 62.822 375.045 62.822Z" stroke="white" strokeWidth="3" />
                <path d="M412.884 71.87C409.23 71.87 406.098 71.203 403.488 69.869C400.936 68.535 398.964 66.621 397.572 64.127C396.18 61.633 395.484 58.617 395.484 55.079V36.635C395.484 33.039 396.18 29.994 397.572 27.5C398.964 25.006 400.936 23.121 403.488 21.845C406.098 20.511 409.23 19.844 412.884 19.844C416.48 19.844 419.525 20.395 422.019 21.497C424.571 22.599 426.514 24.281 427.848 26.543C429.182 28.747 429.849 31.56 429.849 34.982V40.289H416.19V34.547C416.19 33.213 416.045 32.198 415.755 31.502C415.523 30.748 415.146 30.226 414.624 29.936C414.16 29.646 413.58 29.501 412.884 29.501C412.188 29.501 411.608 29.704 411.144 30.11C410.68 30.458 410.332 31.067 410.1 31.937C409.868 32.749 409.752 33.909 409.752 35.417V56.297C409.752 58.559 410.013 60.096 410.535 60.908C411.115 61.72 411.927 62.126 412.971 62.126C413.725 62.126 414.334 61.981 414.798 61.691C415.262 61.343 415.61 60.821 415.842 60.125C416.074 59.371 416.19 58.385 416.19 57.167V50.468H429.849V56.558C429.849 59.922 429.153 62.764 427.761 65.084C426.427 67.346 424.484 69.057 421.932 70.217C419.438 71.319 416.422 71.87 412.884 71.87Z" stroke="white" strokeWidth="3" />
                <path d="M442.357 71.87C439.805 71.87 437.659 71.261 435.919 70.043C434.179 68.825 432.874 67.288 432.004 65.432C431.134 63.518 430.699 61.575 430.699 59.603C430.699 56.471 431.308 53.832 432.526 51.686C433.802 49.54 435.455 47.742 437.485 46.292C439.515 44.842 441.748 43.624 444.184 42.638C446.62 41.594 449.027 40.666 451.405 39.854V34.808C451.405 33.938 451.318 33.184 451.144 32.546C450.97 31.85 450.68 31.328 450.274 30.98C449.868 30.574 449.259 30.371 448.447 30.371C447.635 30.371 446.997 30.545 446.533 30.893C446.127 31.241 445.837 31.734 445.663 32.372C445.489 32.952 445.373 33.619 445.315 34.373L444.967 38.027L431.743 37.505C432.033 31.473 433.628 27.036 436.528 24.194C439.486 21.294 443.894 19.844 449.752 19.844C454.856 19.844 458.655 21.265 461.149 24.107C463.701 26.891 464.977 30.516 464.977 34.982V58.472C464.977 60.56 465.006 62.387 465.064 63.953C465.18 65.519 465.296 66.882 465.412 68.042C465.586 69.202 465.731 70.188 465.847 71H453.145C452.971 69.666 452.768 68.216 452.536 66.65C452.304 65.084 452.13 64.156 452.014 63.866C451.434 65.954 450.332 67.81 448.708 69.434C447.142 71.058 445.025 71.87 442.357 71.87ZM447.403 62.3C447.983 62.3 448.505 62.184 448.969 61.952C449.491 61.72 449.955 61.401 450.361 60.995C450.767 60.589 451.115 60.183 451.405 59.777V46.031C450.245 46.727 449.172 47.452 448.186 48.206C447.2 48.902 446.359 49.685 445.663 50.555C444.967 51.425 444.416 52.382 444.01 53.426C443.662 54.412 443.488 55.543 443.488 56.819C443.488 58.501 443.836 59.835 444.532 60.821C445.228 61.807 446.185 62.3 447.403 62.3Z" stroke="white" strokeWidth="3" />
                <path d="M483.646 71.87C479.238 71.87 475.555 70.884 472.597 68.912C469.697 66.94 467.319 63.721 465.463 59.255L475.12 54.992C476.048 57.254 477.121 59.052 478.339 60.386C479.615 61.662 481.007 62.3 482.515 62.3C483.617 62.3 484.429 62.068 484.951 61.604C485.531 61.14 485.821 60.502 485.821 59.69C485.821 58.356 485.299 57.138 484.255 56.036C483.269 54.934 481.471 53.31 478.861 51.164L475.207 48.119C472.539 45.857 470.422 43.682 468.856 41.594C467.348 39.448 466.594 36.809 466.594 33.677C466.594 30.951 467.29 28.544 468.682 26.456C470.132 24.368 472.046 22.744 474.424 21.584C476.802 20.424 479.354 19.844 482.08 19.844C486.198 19.844 489.649 20.975 492.433 23.237C495.217 25.499 497.16 28.689 498.262 32.807L489.127 36.983C488.779 35.881 488.286 34.779 487.648 33.677C487.01 32.575 486.227 31.647 485.299 30.893C484.429 30.139 483.472 29.762 482.428 29.762C481.5 29.762 480.746 30.023 480.166 30.545C479.644 31.067 479.383 31.763 479.383 32.633C479.383 33.735 479.992 34.924 481.21 36.2C482.486 37.476 484.226 39.013 486.43 40.811L489.997 43.943C491.447 45.161 492.868 46.466 494.26 47.858C495.652 49.25 496.783 50.787 497.653 52.469C498.581 54.151 499.045 56.065 499.045 58.211C499.045 61.111 498.32 63.605 496.87 65.693C495.42 67.723 493.506 69.26 491.128 70.304C488.808 71.348 486.314 71.87 483.646 71.87Z" stroke="white" strokeWidth="3" />
                <path d="M515.386 71.522C511.616 71.522 508.687 70.971 506.599 69.869C504.569 68.767 503.177 67.172 502.423 65.084C501.669 62.938 501.292 60.357 501.292 57.341V30.023H496.42V20.714H501.292V5.40203H515.647V20.714H522.781V30.023H515.647V55.949C515.647 57.399 516.053 58.443 516.865 59.081C517.735 59.719 518.808 60.038 520.084 60.038C520.838 60.038 521.534 60.009 522.172 59.951C522.81 59.835 523.39 59.748 523.912 59.69V70.826C523.158 70.942 521.969 71.087 520.345 71.261C518.721 71.435 517.068 71.522 515.386 71.522Z" stroke="white" strokeWidth="3" />
              </svg>

              {/* Titre 1 petit écran */}
              <svg id="logo" className="sm:hidden" width="318" height="48" viewBox="0 0 318 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M7.06391 39L0.0559082 0.119995H7.39991L11.5759 26.52L15.4639 0.119995H23.2399L16.1359 39H7.06391Z" stroke="white" strokeWidth="2" />
                <path d="M34.5912 39.48C32.5752 39.48 30.8472 39.128 29.4072 38.424C27.9672 37.688 26.8632 36.632 26.0952 35.256C25.3272 33.848 24.9432 32.168 24.9432 30.216V20.04C24.9432 18.056 25.3272 16.376 26.0952 15C26.8632 13.624 27.9672 12.584 29.4072 11.88C30.8792 11.144 32.6072 10.776 34.5912 10.776C36.6392 10.776 38.3352 11.128 39.6792 11.832C41.0552 12.536 42.0792 13.576 42.7512 14.952C43.4552 16.328 43.8072 18.024 43.8072 20.04V25.608H32.6712V30.936C32.6712 31.64 32.7512 32.216 32.9112 32.664C33.0712 33.112 33.2952 33.432 33.5832 33.624C33.8712 33.816 34.2072 33.912 34.5912 33.912C34.9752 33.912 35.3112 33.832 35.5992 33.672C35.9192 33.48 36.1592 33.192 36.3192 32.808C36.5112 32.424 36.6072 31.896 36.6072 31.224V28.296H43.8072V30.984C43.8072 33.8 42.9912 35.928 41.3592 37.368C39.7272 38.776 37.4712 39.48 34.5912 39.48ZM32.6712 21.96H36.6072V19.176C36.6072 18.44 36.5112 17.864 36.3192 17.448C36.1592 17.032 35.9192 16.744 35.5992 16.584C35.3112 16.424 34.9592 16.344 34.5432 16.344C34.1592 16.344 33.8232 16.44 33.5352 16.632C33.2792 16.824 33.0712 17.16 32.9112 17.64C32.7512 18.088 32.6712 18.744 32.6712 19.608V21.96Z" stroke="white" strokeWidth="2" />
                <path d="M47.202 39V11.256H55.026V15.864C55.922 14.264 56.818 13.048 57.714 12.216C58.61 11.352 59.698 10.92 60.978 10.92C61.234 10.92 61.458 10.936 61.65 10.968C61.842 10.968 62.018 11 62.178 11.064V18.84C61.826 18.68 61.426 18.552 60.978 18.456C60.562 18.328 60.114 18.264 59.634 18.264C58.738 18.264 57.906 18.504 57.138 18.984C56.37 19.464 55.666 20.12 55.026 20.952V39H47.202Z" stroke="white" strokeWidth="2" />
                <path d="M65.289 47.064V41.64C66.281 41.64 67.097 41.576 67.737 41.448C68.409 41.32 68.905 41.08 69.225 40.728C69.577 40.376 69.753 39.896 69.753 39.288C69.753 38.744 69.657 38.088 69.465 37.32C69.273 36.52 69.033 35.624 68.745 34.632L62.217 11.256H69.561L73.065 27.288L75.705 11.256H83.097L76.713 39.672C76.297 41.592 75.641 43.08 74.745 44.136C73.849 45.224 72.745 45.976 71.433 46.392C70.153 46.84 68.697 47.064 67.065 47.064H65.289Z" stroke="white" strokeWidth="2" />
                <path d="M87.7167 39L83.6367 11.256H90.1647L92.1807 27L94.3407 11.256H100.005L101.877 27.384L104.229 11.256H110.277L106.149 39H99.4287L97.0287 23.4L94.3407 39H87.7167Z" stroke="white" strokeWidth="2" />
                <path d="M121.829 39.48C119.813 39.48 118.085 39.128 116.645 38.424C115.205 37.688 114.101 36.632 113.333 35.256C112.565 33.848 112.181 32.168 112.181 30.216V20.04C112.181 18.056 112.565 16.376 113.333 15C114.101 13.624 115.205 12.584 116.645 11.88C118.117 11.144 119.845 10.776 121.829 10.776C123.877 10.776 125.573 11.128 126.917 11.832C128.293 12.536 129.317 13.576 129.989 14.952C130.693 16.328 131.045 18.024 131.045 20.04V25.608H119.909V30.936C119.909 31.64 119.989 32.216 120.149 32.664C120.309 33.112 120.533 33.432 120.821 33.624C121.109 33.816 121.445 33.912 121.829 33.912C122.213 33.912 122.549 33.832 122.837 33.672C123.157 33.48 123.397 33.192 123.557 32.808C123.749 32.424 123.845 31.896 123.845 31.224V28.296H131.045V30.984C131.045 33.8 130.229 35.928 128.597 37.368C126.965 38.776 124.709 39.48 121.829 39.48ZM119.909 21.96H123.845V19.176C123.845 18.44 123.749 17.864 123.557 17.448C123.397 17.032 123.157 16.744 122.837 16.584C122.549 16.424 122.197 16.344 121.781 16.344C121.397 16.344 121.061 16.44 120.773 16.632C120.517 16.824 120.309 17.16 120.149 17.64C119.989 18.088 119.909 18.744 119.909 19.608V21.96Z" stroke="white" strokeWidth="2" />
                <path d="M134.68 39V0.119995H142.456V39H134.68Z" stroke="white" strokeWidth="2" />
                <path d="M146.892 39V0.119995H154.668V39H146.892Z" stroke="white" strokeWidth="2" />
                <path d="M158.528 39V31.608H165.92V39H158.528Z" stroke="white" strokeWidth="2" />
                <path d="M169.959 39V0.119995H183.015C185.351 0.119995 187.271 0.567995 188.775 1.46399C190.279 2.328 191.399 3.608 192.135 5.304C192.871 6.968 193.239 9 193.239 11.4C193.239 14.408 192.743 16.712 191.751 18.312C190.759 19.88 189.415 20.968 187.719 21.576C186.023 22.184 184.119 22.488 182.007 22.488H178.551V39H169.959ZM178.551 16.488H181.431C182.487 16.488 183.287 16.296 183.831 15.912C184.375 15.528 184.727 14.952 184.887 14.184C185.079 13.416 185.175 12.44 185.175 11.256C185.175 10.264 185.095 9.4 184.935 8.664C184.807 7.896 184.471 7.288 183.927 6.84C183.383 6.392 182.535 6.168 181.383 6.168H178.551V16.488Z" stroke="white" strokeWidth="2" />
                <path d="M205.062 39.48C203.078 39.48 201.35 39.144 199.878 38.472C198.438 37.8 197.318 36.792 196.518 35.448C195.75 34.104 195.366 32.424 195.366 30.408V19.848C195.366 17.832 195.75 16.152 196.518 14.808C197.318 13.464 198.438 12.456 199.878 11.784C201.35 11.112 203.078 10.776 205.062 10.776C207.046 10.776 208.758 11.112 210.198 11.784C211.67 12.456 212.806 13.464 213.606 14.808C214.406 16.152 214.806 17.832 214.806 19.848V30.408C214.806 32.424 214.406 34.104 213.606 35.448C212.806 36.792 211.67 37.8 210.198 38.472C208.758 39.144 207.046 39.48 205.062 39.48ZM205.11 34.296C205.622 34.296 206.022 34.168 206.31 33.912C206.598 33.624 206.79 33.24 206.886 32.76C207.014 32.248 207.078 31.64 207.078 30.936V19.32C207.078 18.616 207.014 18.024 206.886 17.544C206.79 17.032 206.598 16.648 206.31 16.392C206.022 16.104 205.622 15.96 205.11 15.96C204.598 15.96 204.198 16.104 203.91 16.392C203.622 16.648 203.414 17.032 203.286 17.544C203.158 18.024 203.094 18.616 203.094 19.32V30.936C203.094 31.64 203.158 32.248 203.286 32.76C203.414 33.24 203.622 33.624 203.91 33.912C204.198 34.168 204.598 34.296 205.11 34.296Z" stroke="white" strokeWidth="2" />
                <path d="M224.185 39.48C222.137 39.48 220.553 38.76 219.433 37.32C218.313 35.88 217.753 33.752 217.753 30.936V19.464C217.753 17.736 217.993 16.232 218.473 14.952C218.953 13.64 219.673 12.616 220.633 11.88C221.593 11.144 222.777 10.776 224.185 10.776C225.209 10.776 226.185 10.984 227.113 11.4C228.041 11.784 228.889 12.312 229.657 12.984V0.119995H237.481V39H229.657V36.84C228.793 37.672 227.897 38.328 226.969 38.808C226.041 39.256 225.113 39.48 224.185 39.48ZM227.593 34.488C227.849 34.488 228.153 34.424 228.505 34.296C228.889 34.168 229.273 33.992 229.657 33.768V16.248C229.337 16.088 229.001 15.944 228.649 15.816C228.297 15.688 227.961 15.624 227.641 15.624C226.937 15.624 226.425 15.928 226.105 16.536C225.785 17.112 225.625 17.816 225.625 18.648V31.416C225.625 31.992 225.689 32.52 225.817 33C225.945 33.448 226.153 33.816 226.441 34.104C226.729 34.36 227.113 34.488 227.593 34.488Z" stroke="white" strokeWidth="2" />
                <path d="M250.389 39.48C248.373 39.48 246.645 39.112 245.205 38.376C243.797 37.64 242.709 36.584 241.941 35.208C241.173 33.832 240.789 32.168 240.789 30.216V20.04C240.789 18.056 241.173 16.376 241.941 15C242.709 13.624 243.797 12.584 245.205 11.88C246.645 11.144 248.373 10.776 250.389 10.776C252.373 10.776 254.053 11.08 255.429 11.688C256.837 12.296 257.909 13.224 258.645 14.472C259.381 15.688 259.749 17.24 259.749 19.128V22.056H252.213V18.888C252.213 18.152 252.133 17.592 251.973 17.208C251.845 16.792 251.637 16.504 251.349 16.344C251.093 16.184 250.773 16.104 250.389 16.104C250.005 16.104 249.685 16.216 249.429 16.44C249.173 16.632 248.981 16.968 248.853 17.448C248.725 17.896 248.661 18.536 248.661 19.368V30.888C248.661 32.136 248.805 32.984 249.093 33.432C249.413 33.88 249.861 34.104 250.437 34.104C250.853 34.104 251.189 34.024 251.445 33.864C251.701 33.672 251.893 33.384 252.021 33C252.149 32.584 252.213 32.04 252.213 31.368V27.672H259.749V31.032C259.749 32.888 259.365 34.456 258.597 35.736C257.861 36.984 256.789 37.928 255.381 38.568C254.005 39.176 252.341 39.48 250.389 39.48Z" stroke="white" strokeWidth="2" />
                <path d="M268.571 39.48C267.163 39.48 265.979 39.144 265.019 38.472C264.059 37.8 263.339 36.952 262.859 35.928C262.379 34.872 262.139 33.8 262.139 32.712C262.139 30.984 262.475 29.528 263.147 28.344C263.851 27.16 264.763 26.168 265.883 25.368C267.003 24.568 268.235 23.896 269.579 23.352C270.923 22.776 272.251 22.264 273.563 21.816V19.032C273.563 18.552 273.515 18.136 273.419 17.784C273.323 17.4 273.163 17.112 272.939 16.92C272.715 16.696 272.379 16.584 271.931 16.584C271.483 16.584 271.131 16.68 270.875 16.872C270.651 17.064 270.491 17.336 270.395 17.688C270.299 18.008 270.235 18.376 270.203 18.792L270.011 20.808L262.715 20.52C262.875 17.192 263.755 14.744 265.355 13.176C266.987 11.576 269.419 10.776 272.651 10.776C275.467 10.776 277.563 11.56 278.939 13.128C280.347 14.664 281.051 16.664 281.051 19.128V32.088C281.051 33.24 281.067 34.248 281.099 35.112C281.163 35.976 281.227 36.728 281.291 37.368C281.387 38.008 281.467 38.552 281.531 39H274.523C274.427 38.264 274.315 37.464 274.187 36.6C274.059 35.736 273.963 35.224 273.899 35.064C273.579 36.216 272.971 37.24 272.075 38.136C271.211 39.032 270.043 39.48 268.571 39.48ZM271.355 34.2C271.675 34.2 271.963 34.136 272.219 34.008C272.507 33.88 272.763 33.704 272.987 33.48C273.211 33.256 273.403 33.032 273.563 32.808V25.224C272.923 25.608 272.331 26.008 271.787 26.424C271.243 26.808 270.779 27.24 270.395 27.72C270.011 28.2 269.707 28.728 269.483 29.304C269.291 29.848 269.195 30.472 269.195 31.176C269.195 32.104 269.387 32.84 269.771 33.384C270.155 33.928 270.683 34.2 271.355 34.2Z" stroke="white" strokeWidth="2" />
                <path d="M293.271 39.48C290.839 39.48 288.807 38.936 287.175 37.848C285.575 36.76 284.263 34.984 283.239 32.52L288.567 30.168C289.079 31.416 289.671 32.408 290.343 33.144C291.047 33.848 291.815 34.2 292.647 34.2C293.255 34.2 293.703 34.072 293.991 33.816C294.311 33.56 294.471 33.208 294.471 32.76C294.471 32.024 294.183 31.352 293.607 30.744C293.063 30.136 292.071 29.24 290.631 28.056L288.615 26.376C287.143 25.128 285.975 23.928 285.111 22.776C284.279 21.592 283.863 20.136 283.863 18.408C283.863 16.904 284.247 15.576 285.015 14.424C285.815 13.272 286.871 12.376 288.183 11.736C289.495 11.096 290.903 10.776 292.407 10.776C294.679 10.776 296.583 11.4 298.119 12.648C299.655 13.896 300.727 15.656 301.335 17.928L296.295 20.232C296.103 19.624 295.831 19.016 295.479 18.408C295.127 17.8 294.695 17.288 294.183 16.872C293.703 16.456 293.175 16.248 292.599 16.248C292.087 16.248 291.671 16.392 291.351 16.68C291.063 16.968 290.919 17.352 290.919 17.832C290.919 18.44 291.255 19.096 291.927 19.8C292.631 20.504 293.591 21.352 294.807 22.344L296.775 24.072C297.575 24.744 298.359 25.464 299.127 26.232C299.895 27 300.519 27.848 300.999 28.776C301.511 29.704 301.767 30.76 301.767 31.944C301.767 33.544 301.367 34.92 300.567 36.072C299.767 37.192 298.711 38.04 297.399 38.616C296.119 39.192 294.743 39.48 293.271 39.48Z" stroke="white" strokeWidth="2" />
                <path d="M312.702 39.288C310.622 39.288 309.006 38.984 307.854 38.376C306.734 37.768 305.966 36.888 305.55 35.736C305.134 34.552 304.926 33.128 304.926 31.464V16.392H302.238V11.256H304.926V2.808H312.846V11.256H316.782V16.392H312.846V30.696C312.846 31.496 313.07 32.072 313.518 32.424C313.998 32.776 314.59 32.952 315.294 32.952C315.71 32.952 316.094 32.936 316.446 32.904C316.798 32.84 317.118 32.792 317.406 32.76V38.904C316.99 38.968 316.334 39.048 315.438 39.144C314.542 39.24 313.63 39.288 312.702 39.288Z" stroke="white" strokeWidth="2" />
              </svg>

              <h2 className="title2-home text-3xl sm:text-5xl">Ecoutez le numérique</h2>
            </div>

            {/* CTA Principal + découvrir */}
            <div className="absolute bottom-0 left-1/2 transform -translate-x-1/2">
              <div className="pulse2 rounded-full">
                <div className="pulse3 rounded-full">
                  <Link href="/podcasts" className="pulse font-bold px-8 py-4 sm:px-12 sm:py-6 rounded-full inline-flex justify-center items-center w-80 sm:w-full" style={{ backgroundImage: "var(--linear-primary)" }}>Ecoutez nos Podcasts <span className="ps-5"><MdHeadphones size={36} /></span></Link>
                </div>
              </div>
              <Link href="#actu" className="arow-hover flex flex-col items-center pb-3 pt-11">
                <p className="text-xs text-center pb-2">Découvrir</p>
                <HiOutlineArrowNarrowDown size={20} className="arow" />
              </Link>
            </div>

            {/* Liste réseaux sociaux */}
            <ul className="absolute top-1/2 right-0 transform -translate-x-1/2 -translate-y-1/2 flex flex-col gap-y-8 pe-1 md:pe-4 hidden sm:flex">
              <li>
                <Link href=""><BiLogoFacebook size={20} /></Link>
              </li>
              <li>
                <Link href=""><BiLogoLinkedin size={20} /></Link>
              </li>
              <li>
                <Link href=""><BiLogoTwitter size={20} /></Link>
              </li>
              <li>
                <Link href=""><BiLogoInstagram size={20} /></Link>
              </li>
            </ul>

          </section>
        </div>

        {/* SECTION ACTUALITES */}
        <section id="actu" className="w-4/5 mx-auto py-12 lg:pt-16 lg:pb-28">
          <p className="barre relative uppercase text-neutral-400"><span className="ms-7">Actualités</span></p>
          <h2 className="text-2xl uppercase font-semibold pb-4">Nos derniers épisodes</h2>
          <div className="pt-10 flex gap-16">

            {/* Liste actualités petit écran */}
            <div className="md:hidden podcastActu w-1/4 flex flex-row md:flex-col flex-1 overflow-auto md:h-screen snap-mandatory snap-x">
              {lastPodcasts.map((podcast) => (
                <Link href={`${process.env.NEXT_PUBLIC_APP_URL}/podcasts/${podcast.attributes.slug}`} id="1" className="h-full min-w-full pb-6 snap-center" key={podcast.id}>
                  <div className="actuSm h-3/4 rounded-md object-cover" style={{ backgroundImage: `url(${process.env.NEXT_PUBLIC_API_URL}${podcast.attributes.img.data.attributes.url})`, backgroundPosition: 'center', backgroundSize: 'cover', filter: 'grayscale(40%)' }}></div>
                  <div className="h-1/4 flex py-3">
                    <div className="pe-4"><BsPlayCircleFill size={55} /></div>
                    <div>
                      <p className="uppercase">Episode #{podcast?.attributes?.episode}</p>
                      <p className="text-xl">{podcast?.attributes?.titre}</p>
                      <p className="text-sm text-zinc-400">{podcast?.attributes?.date}</p>
                    </div>
                  </div>
                </Link>
              ))}
            </div>

            {/* Liste actualités grand écran */}
            <div className="hidden md:flex podcastActu w-1/4 flex flex-row md:flex-col flex-1 overflow-auto md:h-screen">
              {lastPodcastsMd.map((podcast) => (
                <Link href={`${process.env.NEXT_PUBLIC_APP_URL}/podcasts/${podcast.attributes.slug}`} id="1" className="h-full min-w-full pb-6" key={podcast.id}>
                  <div className="actuMd h-3/4 rounded-md object-cover" style={{ backgroundImage: `url(${process.env.NEXT_PUBLIC_API_URL}${podcast.attributes.img.data.attributes.url})`, backgroundPosition: 'center', backgroundSize: 'cover', filter: 'grayscale(40%)' }}></div>
                  <div className="h-1/4 flex py-3">
                    <div className="pe-4"><BsPlayCircleFill size={55} /></div>
                    <div>
                      <p className="uppercase">Episode #{podcast?.attributes?.episode}</p>
                      <p className="text-xl">{podcast?.attributes?.titre}</p>
                      <p className="text-sm text-zinc-400">{podcast?.attributes?.date}</p>
                    </div>
                  </div>
                </Link>
              ))}
            </div>

            {/* Dernière actualité */}
            <Link
              href={`${process.env.NEXT_PUBLIC_APP_URL}/podcasts/${lastPodcast?.attributes.slug}`}
              className="hidden md:block relative w-3/4 flex-1 text-white rounded-md overflow-hidden"
              style={{
                backgroundImage: `url(${process.env.NEXT_PUBLIC_API_URL}${lastPodcast?.attributes?.img?.data?.attributes?.url})`,
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                filter: 'grayscale(40%)'
              }}
            >
              <div className="absolute bottom-0 w-full h-1/2 rounded-md" style={{ background: 'linear-gradient(180deg, rgba(25, 28, 38, 0), rgba(25, 28, 38, 0.8))' }}></div>
              <div className="h-full pb-6">

                <div className="absolute flex items-center py-10 px-8 bottom-0">
                  <div className="pe-10"><BsPlayCircleFill size={70} /></div>
                  <div>
                    <p className="uppercase">Episode #{lastPodcast?.attributes?.episode}</p>
                    <p className="text-4xl py-1">{lastPodcast?.attributes?.titre}</p>
                    <p className="text-sm">{lastPodcast?.attributes?.date}</p>
                  </div>
                </div>
              </div>
            </Link>
          </div>
        </section>

        {/* SECTION ECOUTER TOUS LES PODCASTS */}
        <div className="ecouter relative text-white overflow-hidden	flex flex-col justify-center" style={{ backgroundImage: `url(/img/accueil/ecouter.jpg)`, backgroundPosition: 'center', backgroundSize: 'cover' }}>
          <div className="absolute h-full w-full bg-zinc-900 bg-opacity-50"></div>

          {/* Bouton play en fonction des différentes tailles d'écran */}
          <Link href="/podcasts" className="pulse absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-10 rounded-full">
            <div className="pulse2 rounded-full">
              <div className="pulse3 rounded-full">
                <BsPlayCircle size={60} className="inline-block sm:hidden rounded-full " style={{ backgroundImage: "var(--linear-primary)" }} />
                <BsPlayCircle size={75} className="hidden sm:inline-block md:hidden rounded-full" style={{ backgroundImage: "var(--linear-primary)" }} />
                <BsPlayCircle size={90} className="hidden md:inline-block lg:hidden rounded-full" style={{ backgroundImage: "var(--linear-primary)" }} />
                <BsPlayCircle size={100} className="hidden lg:inline-block xl:hidden rounded-full" style={{ backgroundImage: "var(--linear-primary)" }} />
                <BsPlayCircle size={120} className="hidden xl:inline-block 2xl:hidden rounded-full" style={{ backgroundImage: "var(--linear-primary)" }} />
                <BsPlayCircle size={150} className="hidden 2xl:inline-block rounded-full" style={{ backgroundImage: "var(--linear-primary)" }} />
              </div>
            </div>
          </Link>

          {/* Textes défilants */}
          <div>
            <div className="capitalize text-3xl sm:text-4xl md:text-5xl lg:text-6xl pb-2 md:pb-4 lg:pb-6 w-full flex">
              <span className="txt-infinite1">-&nbsp;Ecoutez&nbsp;tous&nbsp;nos&nbsp;podcasts&nbsp;-&nbsp;Ecoutez&nbsp;tous&nbsp;nos&nbsp;podcasts&nbsp;</span>
              <span className="txt-infinite1">-&nbsp;Ecoutez&nbsp;tous&nbsp;nos&nbsp;podcasts&nbsp;-&nbsp;Ecoutez&nbsp;tous&nbsp;nos&nbsp;podcasts&nbsp;</span>
              <span className="txt-infinite1">-&nbsp;Ecoutez&nbsp;tous&nbsp;nos&nbsp;podcasts&nbsp;-&nbsp;Ecoutez&nbsp;tous&nbsp;nos&nbsp;podcasts&nbsp;</span>
            </div>
            <div className="outline-text capitalize text-3xl sm:text-4xl md:text-5xl lg:text-6xl pt-2 md:pt-4 lg:pt-6 h-full w-full flex">
              <span className="txt-infinite2">-&nbsp;Ecoutez&nbsp;tous&nbsp;nos&nbsp;podcasts&nbsp;-&nbsp;Ecoutez&nbsp;tous&nbsp;nos&nbsp;podcasts&nbsp;</span>
              <span className="txt-infinite2">-&nbsp;Ecoutez&nbsp;tous&nbsp;nos&nbsp;podcasts&nbsp;-&nbsp;Ecoutez&nbsp;tous&nbsp;nos&nbsp;podcasts&nbsp;</span>
              <span className="txt-infinite2">-&nbsp;Ecoutez&nbsp;tous&nbsp;nos&nbsp;podcasts&nbsp;-&nbsp;Ecoutez&nbsp;tous&nbsp;nos&nbsp;podcasts&nbsp;</span>
            </div>
          </div>

        </div>

        {/* SECTION ENTREPRISE */}
        <section className="bg-neutral-100 py-20 sm:pt-28 sm:pb-40">
          <div className="px-4 sm:px-0 sm:w-4/5 mx-auto sm:flex justify-between items-center">
            <div className="lg:w-3/5">
              <p className="barre relative uppercase text-neutral-400"><span className="ms-7">A propos de nous</span></p>
              <h2 className="text-2xl uppercase font-semibold pb-4">Notre histoire</h2>
              <h3 className="text-4xl sm:text-5xl font-semibold py-8"><span className="text-gradient">Nous partageons</span> notre passion du numérique <span className="text-gradient">avec vous</span></h3>
              <p className="pt-5 pb-8">Depuis 2000, nous travaillons dans le domaine du numérique au sein d’une agence web, et Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi finibus varius mauris vitae pulvinar. Morbi pretium justo a ipsum consectetur posuere. Praesent tempus ipsum vel libero finibus tempus. Curabitur efficitur et diam in laoreet. Duis eget blandit nibh, ac vestibulum ex.</p>
              <Link href="/a-propos">
                <button className="button-hover capitalize border rounded-full border-neutral-300 px-8 py-4 transition hover:bg-neutral-50">En savoir plus</button>
                </Link>
            </div>
            <div className="py-4 md:py-0 md:w-1/3">
              <img className=" sm:hidden lg:block rounded-xl" src="/img/accueil/nous.jpg" alt="" />
            </div>
          </div>
        </section>

        {/* SECTION COLLABORER */}
        <Collaborate />

        {/* SECTION TEMOIGNAGES */}
        <section className="pt-24 pb-32">
          <div className="px-4 sm:px-0 sm:w-4/5 mx-auto pb-10">
            <p className="barre relative uppercase text-neutral-400"><span className="ms-7">Témoignages</span></p>
            <h2 className="text-2xl uppercase font-semibold pb-4">Votre confiance</h2>
          </div>

          <div className="flex px-4 py-3 sm:px-0 sm:w-4/5 mx-auto gap-6 overflow-x-auto">

            {/* Témoignage 1 */}
            <div className="bg-stone-50 w-1/3 px-6 py-3 rounded-xl shadow-lg min-w-[300px]">
              <div className="flex justify-between items-center pb-3">
                <div className="bg-stone-100 rounded-full p-3">
                  <TbLetterM size={30} />
                </div>
                <RiDoubleQuotesL size={30} />
              </div>
              <p className="text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi finibus varius mauris vitae pulvinar. Morbi pretium justo a ipsum consectetur posuere. Praesent tempus ipsum vel libero finibus tempus. Curabitur efficitur et diam in laoreet.</p>
            </div>

            {/* Témoignage 2 */}
            <div className="bg-stone-50 w-1/3 px-6 py-3 rounded-xl shadow-lg min-w-[300px]">
              <div className="flex justify-between items-center pb-3">
                <div className="bg-stone-100 rounded-full p-3">
                  <TbLetterM size={30} />
                </div>
                <RiDoubleQuotesL size={30} />
              </div>
              <p className="text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi finibus varius mauris vitae pulvinar. Morbi pretium justo a ipsum consectetur posuere. Praesent tempus ipsum vel libero finibus tempus. Curabitur efficitur et diam in laoreet.</p>
            </div>

            {/* Témoignage 3 */}
            <div className="bg-stone-50 w-1/3 px-6 py-3 rounded-xl shadow-lg min-w-[300px]">
              <div className="flex justify-between items-center pb-3">
                <div className="bg-stone-100 rounded-full p-3">
                  <TbLetterM size={30} />
                </div>
                <RiDoubleQuotesL size={30} />
              </div>
              <p className="text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi finibus varius mauris vitae pulvinar. Morbi pretium justo a ipsum consectetur posuere. Praesent tempus ipsum vel libero finibus tempus. Curabitur efficitur et diam in laoreet.</p>
            </div>

          </div>
        </section>

        {/* SECTION NEWSLETTER */}
        <NewsLetter />
      </div>
    </>
  )
}
