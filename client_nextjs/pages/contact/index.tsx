
// =================== PREMIER FORMULAIRE DE CONTACT AVANT L'INTÉGRATION DU TYPEFORM, RELIÉ À LA COLLECTION 'COMMENTAIRE' DE STRAPI ===================


// import Head from 'next/head'
// import { useState } from "react";

// export default function Contact() {

//     // Fonction pour empêcher le comportement par défaut de soumission du formulaire
//     async function handleSubmit(e) {
//         e.preventDefault();
//     }

//     // Stocker les valeurs des champs du formulaire
//     const [nom, setNom] = useState('');
//     const [email, setEmail] = useState('');
//     const [sujet, setSujet] = useState('');
//     const [commentaire, setCommentaire] = useState('');

//     // Gérer l'affichage du message de soumission réussie ou échouée
//     const [isFormSubmitted, setIsFormSubmitted] = useState(undefined);

//     // Fonction pour envoyer les informations du formulaire à l'API
//     async function addContact() {
//         const contactInfo = {
//             nom: nom,
//             email: email,
//             sujet: sujet,
//             commentaire: commentaire
//         };

//         const requestBody = {
//             data: contactInfo
//         };

//         const add = await fetch('${process.env.NEXT_PUBLIC_API_URL}/api/contacts', {
//             method: "POST",
//             headers: {
//                 'Accept': 'application/json',
//                 'Content-Type': 'application/json'
//             },
//             body: JSON.stringify(requestBody)
//         });

//         const addResponse = await add.json();
//         console.log(addResponse);

//         // Vérification de la réponse pour afficher un message de succès ou d'erreur et vider les champs
//         if (add.status === 200) {
//             setNom('');
//             setEmail('');
//             setSujet('');
//             setCommentaire('');

//             setIsFormSubmitted(true);
//         } else {
//             setIsFormSubmitted(false);
//         }
//     }



//     return (

//         <>
//             <Head>
//                 <title> Contactez-nous - Verywell.Podcast </title>
//                 <meta name="description" content="N'hésitez pas à nous contacter pour toute question, suggestion ou demande. Nous sommes là pour vous aider et sommes impatients de vous entendre." />
//                 <link rel="icon" href="/img/logo.png" />
//             </Head>

//             <div className="flex flex-col items-center pt-20 md:pt-28 px-3">

//                 {/* ENTETE */}
//                 <h1 className="text-white text-3xl md:text-4xl">Contactez-nous</h1>
//                 <p className="text-neutral-300 pt-5 pb-3 text-center">Vous souhaitez collaborer? Suggérer un sujet? Autre chose? <br /> Dites-nous tout !</p>

//                 {/* FORMULAIRE */}
//                 <form className="w-full sm:w-3/4 lg:w-1/2" onSubmit={handleSubmit}>
//                     <div className="flex flex-col md:flex-row pb-3 gap-3 md:gap-5">

//                         {/* Champ Nom */}
//                         <div className="flex flex-col w-full md:w-1/2">
//                             <label className="hidden md:block text-white pb-3" htmlFor="nom">Nom</label>
//                             <input
//                                 className="border border-neutral-300 rounded px-4 py-3 w-full"
//                                 type="text"
//                                 name="nom"
//                                 required
//                                 placeholder="Votre nom *"
//                                 minLength={2}
//                                 maxLength={50}
//                                 pattern="^[a-z]+[ \-']?[[a-z]+[ \-']?]*[a-z]+$"
//                                 title="Veuillez entrer un nom valide (2 à 50 caractères, lettres, espaces, apostrophes et tirets autorisés)."
//                                 onChange={e => setNom(e.target.value)}
//                                 value={nom}
//                             />
//                         </div>

//                         {/* Champ Email */}
//                         <div className="flex flex-col w-full md:w-1/2">
//                             <label className="hidden md:block text-white pb-3" htmlFor="email">E-mail</label>
//                             <input
//                                 className="border border-neutral-300 rounded px-4 py-3 w-full"
//                                 type="email"
//                                 name="email"
//                                 required
//                                 placeholder="Votre E-mail *"
//                                 pattern="^[\w\-\+]+(\.[\w\-]+)*@[\w\-]+(\.[\w\-]+)*\.[\w\-]{2,4}$"
//                                 title="Veuillez entrer une adresse e-mail valide (exemple@domaine.com)."
//                                 onChange={e => setEmail(e.target.value)}
//                                 value={email}
//                             />
//                         </div>

//                     </div>

//                     {/* Champ choix Sujet */}
//                     <div className="flex flex-col pb-3">
//                         <label className="hidden md:block text-white pb-3" htmlFor="sujet">Sujet</label>
//                         <select
//                             className="bg-white border border-neutral-300 rounded px-3 py-3 w-full"
//                             name="sujet"
//                             required
//                             onChange={e => setSujet(e.target.value)}
//                             value={sujet}
//                         >
//                             <option className="" value="">Choisissez un sujet *</option>
//                             <option value="collaborer">Collaborer</option>
//                             <option value="offre_commerciale">Offre commerciale</option>
//                             <option value="suggérer_un_sujet">Suggérer un sujet</option>
//                             <option value="autre">Autre</option>
//                         </select>
//                     </div>

//                     {/* Champ Commentaire */}
//                     <div className="flex flex-col w-full">
//                         <label className="hidden md:block text-white pb-3" htmlFor="commentaire">Commentaire</label>
//                         <textarea
//                             className="border border-neutral-300 rounded px-4 py-3 pb-16 md:pb-24 lg:pb-32 w-full"
//                             name="commentaire"
//                             required
//                             placeholder="Votre commentaire *"
//                             minLength={10}
//                             maxLength={500}
//                             title="Veuillez entrer un commentaire valide (10 à 500 caractères)."
//                             onChange={e => setCommentaire(e.target.value)}
//                             value={commentaire}
//                         />
//                     </div>

//                     {/* Message de validation du Formulaire */}
//                     <div className="text-white text-xs py-5">
//                         {/* <input type="radio" id="" name="" value="" required />
//                     <label htmlFor="">J'accepte les conditions d'utilisation etc.</label> */}
//                         {isFormSubmitted === true && <p className="text-lg text-green-500">Votre formulaire a bien été envoyé !</p>}
//                         {isFormSubmitted === false && <p className="text-lg text-red-500">Un problème a été rencontré, veuillez réessayer.</p>}
//                     </div>

//                     {/* Bouton d'envoi du formulaire */}
//                     <div className="flex justify-center">
//                         <input className="button-hover2 text-white px-8 py-4 rounded-full cursor-pointer" type="submit" value="Envoyer" onClick={() => addContact()} style={{ backgroundImage: "var(--linear-primary)" }} />
//                     </div>
//                 </form>
//             </div>
//         </>
//     )
// }


// =================== SECOND FORMULAIRE DE CONTACT AVEC L'INTÉGRATION DU TYPEFORM ===================


import Head from 'next/head'
import { HiOutlineArrowNarrowUp } from 'react-icons/hi';

export default function Contact() {

    return (

        <>
            <Head>
                <title> Contactez-nous - Verywell.Podcast </title>
                <meta name="description" content="N'hésitez pas à nous contacter pour toute question, suggestion ou demande. Nous sommes là pour vous aider et sommes impatients de vous entendre." />
                <link rel="icon" href="/img/logo.png" />
                {/* Intégration du script Typeform */}
                <script src="//embed.typeform.com/next/embed.js" defer></script>
            </Head>
            
            <div className='h-screen relative text-white'>
                <div className="w-full h-full flex flex-col items-center justify-center">

                    {/* Bouton Typeform */}
                    <div className='button-hover2 rounded-full' data-tf-live="01H7DEES833AP9W3SJD9ZW9NFQ"></div>

                    {/* Flèche */}
                    <div className='pt-4 pb-2'>
                        <HiOutlineArrowNarrowUp size={20} className="arow-contact" />
                    </div>

                    {/* Texte */}
                    <h1 className='text-xl md:text-3xl'>Contactez nous, c&apos;est juste ici.</h1>
                </div>
            </div>
        </>
    )
}