import * as React from 'react';
import Head from 'next/head'
import { useRouter } from "next/router";
import { Attributes, useEffect, useState } from "react";
import Link from "next/link";
import AudioPlayer from "@/components/AudioPlayer";
import Share from "../../components/Share"

import { FiDownload } from 'react-icons/fi';

// Type pour définir la structure des données du podcast
type Podcast = {
  date: string;
  description: string;
  duree: string;
  episode: number;
  mp3Lien: string;
  nb_ecoutes?: number;
  titre: string;
  categories: [
    {
      nom: string;
    }
  ];
  hotes: [
    {
      nom: string;
    }
  ];
  participants: [
    {
      nom: string;
      reseau_social: string;
    }
  ];
  img: {
    url: string;
  };
  mp3: {
    url: string;
  };
  horodatages: [
    {
      description: string;
      temps_secondes: number;
    }
  ];
  commentaires: [
    {
      contenu: string;
      date: string;
      mail: string;
      utilisateur: string;
    }
  ];
}



export default function PodcastPage() {

  // Slug du podcast extrait des paramètres de l'URL
  const router = useRouter();
  const { slug } = router.query;

  // Stocker les données du podcast
  const [datas, setData] = useState<Podcast>({
    date: "",
    description: "",
    duree: "",
    episode: 0,
    mp3Lien: "",
    nb_ecoutes: 0,
    titre: "",
    categories: [{
      nom: ""
    }],
    hotes: [{
      nom: ""
    }],
    participants: [{
      nom: "",
      reseau_social: ""
    }],
    img: {
      url: "",
    },
    mp3: {
      url: "",
    },
    horodatages: [{
      description: "string",
      temps_secondes: 0
    }],
    commentaires: [{
      contenu: "",
      date: "",
      mail: "",
      utilisateur: ""
    }],
  });

  // Stocker l'URL du fichier audio
  const [audioUrl, setAudioUrl] = useState<string>("");
  // Stocker le saut de temps dans l'audio 
  const [timeJump, setTimeJump] = useState(0);
  // Gérer le chargement du script Disqus
  const [disqusLoaded, setDisqusLoaded] = useState(false); // Variable Disqus

  // Récupérer les données du podcast depuis l'API
  useEffect(() => {
    if (slug) {
      fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/podcasts/${slug}`)
        .then(response => response.json())
        .then(data => {
          setData(data); // Mettre à jour avec les données directes
          setAudioUrl(data.mp3.url); // Mettre à jour avec la nouvelle structure
        })
        .catch(error => {
          console.error(error);
        });
    }

    // Charger le script Disqus lorsque le composant est monté
    const disqusScript = document.createElement('script');
    disqusScript.src = 'https://verywell-podcast.disqus.com/embed.js';
    disqusScript.setAttribute('data-timestamp', String(Date.now()));
    disqusScript.onload = () => setDisqusLoaded(true);
    document.body.appendChild(disqusScript);

    return () => {
      // Supprimer le script Disqus lorsque le composant est démonté
      document.body.removeChild(disqusScript);
    };

  }, [slug]);

  // Fonction pour gérer le saut dans le temps de l'audio
  const handleTimeJump = (time: number) => {
    setTimeJump(time);
  };

  // Fonction pour formater le temps en heure:minutes:secondes
  const calculateTime = (secs: number) => {
    const hours = Math.floor(secs / 3600);
    const minutes = Math.floor((secs % 3600) / 60);
    const seconds = Math.floor(secs % 60);

    const formattedHours = hours < 10 ? `0${hours}` : `${hours}`;
    const formattedMinutes = minutes < 10 ? `0${minutes}` : `${minutes}`;
    const formattedSeconds = seconds < 10 ? `0${seconds}` : `${seconds}`;

    // Affichage du temps des timestamps
    if (secs < 3600) {
      return `${formattedMinutes}:${formattedSeconds}`;
    } else {
      return `${formattedHours}:${formattedMinutes}:${formattedSeconds}`;
    }
  };

  // Fonction de téléchargement du podcast
  const handleDownload = async () => {
    const audioBlob = await fetch(`${process.env.NEXT_PUBLIC_API_URL}${audioUrl}`).then((response) => response.blob());
    const url = URL.createObjectURL(audioBlob);
    const link = document.createElement('a');
    link.href = url;
    link.download = `Verywell.Podcast Episode ${datas.episode}: ${datas.titre}`; // Remplacez par le nom que vous voulez donner au fichier lors du téléchargement
    link.target = '_blank'; // Ouvrir le lien de téléchargement dans un nouvel onglet
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <>
      <Head>
        <title>Écoutez {datas.titre} | Verywell.Podcast</title>
        <meta name="description" content={`Plongez dans l'épisode ${datas.titre} de notre série de podcasts sur le numérique. Écoutez les discussions fascinantes animées par notre équipe d'experts et nos invités spéciaux.`} />
        <meta property="og:title" content={`Écoutez ${datas.titre} | Verywell.Podcast`} />
        <meta property="og:description" content={`Plongez dans l'épisode ${datas.titre} de notre série de podcasts sur le numérique. Écoutez les discussions fascinantes animées par notre équipe d'experts et nos invités spéciaux.`} />
        <meta property="og:url" content={`${process.env.NEXT_PUBLIC_APP_URL}/${slug}`} />
        <meta property="og:type" content="article" />
        <link rel="icon" href="/img/logo.png" />
      </Head>

      {/* ENTETE */}
      <section className="h-screen sm:h-min text-white py-28 flex justify-center items-center relative" style={{ backgroundImage: `url(/img/podcasts/hero-podcast.jpg)`, backgroundPosition: 'center', backgroundSize: 'cover' }}>
        <div className="absolute h-full w-full bg-zinc-900 bg-opacity-50"></div>
        <div className="gg w-10/12 relative flex justify-center lg:justify-between gap-10">
          <div className="py-6  ">

            {/* Infos Podcast */}
            <h1 className="text-center sm:text-start text-3xl md:text-4xl xl lg:text-5xl">Episode {datas?.episode}: {datas?.titre}</h1>
            <div className="flex flex-wrap justify-center sm:justify-start text-sm uppercase py-5">
              <ul className="flex flex-nowrap overflow-x-auto gap-3 pe-3">
                {datas?.categories?.map((categorie, index) =>
                  <li key={index}>{categorie.nom}</li>
                )}
              </ul>
              <p>&nbsp; - &nbsp;</p>
              <p>{datas?.date}</p>
            </div>

            {/* Lecteur */}
            <div className="pt-10 sm:pt-5">
              <AudioPlayer timeJump={timeJump} podcastData={datas} />
              <button
                className="mx-auto sm:mx-0 text-sm flex items-center justify-center sm:justify-start pt-10 sm:pt-5"
                onClick={handleDownload}
              >
                Télécharger et écouter hors ligne <span className="ps-3"><FiDownload size={20} /></span>
              </button>
            </div>

          </div>

          {/* Image Podcast */}
          <div className="div-square relative hidden lg:block rounded-xl" style={{ backgroundImage: `url(${process.env.NEXT_PUBLIC_API_URL}${datas?.img?.url})`, backgroundPosition: 'center', backgroundSize: 'cover' }}>
            <div className="absolute h-full w-full bg-zinc-900 bg-opacity-30 rounded-xl"></div>
          </div>
        </div>
      </section>

      {/* INFOS PODCAST */}
      <div className="bg-stone-50	pt-12 px-4 sm:px-0 flex flex-col items-center lg:block">
        <div className="flex flex-col lg:flex-row sm:w-10/12 lg:w-full">

          {/* Liste icones de partage */}
          <Share description={"this is a basic share page"} podcastSlug={slug} />

          <section className="podcast-main">

            {/* Timestamps */}
            <p className="barre relative uppercase text-neutral-400"><span className="ms-7">Sommaire</span></p>
            <h2 className="text-2xl uppercase font-semibold pb-4">Les sujets du Podcast</h2>
            <ul className="custom-ul text-sm sm:text-base bg-white text-neutral-500 px-2 sm:px-7 sm:py-10">
              {datas?.horodatages?.map((time) => (
                <li key={time.temps_secondes} className="button-hover py-3 cursor-pointer" onClick={() => handleTimeJump(time.temps_secondes)}>
                  <div className='flex items-center'>
                    <div>
                      <span className="text-linear border-linear px-4 py-2 text-sm">{calculateTime(time.temps_secondes)}</span>
                    </div>
                    <div className='ps-2'> - {time.description}</div>
                  </div>
                </li>
              ))}
            </ul>

            {/* Description Podcast */}
            <p className="text-neutral-700 py-8 md:pt-16 md:pb-24">
              {datas?.description.split('\n').map((paragraph, index) => (
                <React.Fragment key={index}>
                  {paragraph}
                  <br />
                </React.Fragment>
              ))}
            </p>
          </section>

          {/* Hôtes + Invités */}
          <div className="flex gap-20 lg:block podcast-people lg:ps-24 lg:pt-16 mt-1">
            <div className="pb-20">
              <h3 className="uppercase pb-5">Invités</h3>
              <ul>
                {datas?.participants?.map((participant, index) =>
                  <li key={index} className="text-linear"><Link href={`${participant.reseau_social}`}>{participant.nom}</Link></li>
                )}
              </ul>
            </div>
            <div>
              <h3 className="uppercase pb-5">Hôtes</h3>
              <ul>
                {datas?.hotes?.map((hote, index) =>
                  <li key={index}>{hote.nom}</li>
                )}
              </ul>
            </div>
          </div>

        </div>


        {/* SECTION COMMENTAIRES */}
        <div className="flex sm:w-10/12 w-full lg:w-full">
          <div className="podcast-void"></div>
          <section className="podcast-comments w-full">
            <p className="barre relative uppercase text-neutral-400"><span className="ms-7">Participer</span></p>
            <h2 className="text-2xl uppercase font-semibold pb-7">Laisser un commentaire</h2>

            {/* Section commentaire Discuss */}
            <div className="py-16" id="disqus_thread"></div>
          </section>
        </div>
      </div>
    </>
  );
}



