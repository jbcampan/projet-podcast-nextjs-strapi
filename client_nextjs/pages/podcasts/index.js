import Head from 'next/head'
import Card from "../../components/Card";
import { useApi } from '../../context/apiContext';
import { FiChevronLeft } from 'react-icons/fi';
import { FiChevronRight } from "react-icons/fi";
import { useEffect, useState } from "react";


export default function Podcasts() {

    // Récupérer des podcasts de apiContext
    const { datas } = useApi();
    // Stocker les catégories de manière unique 
    const [categories, setCategories] = useState(new Set());
    // Stocker les catégories sélectionnées par l'utilisateur
    const [selectedCategories, setSelectedCategories] = useState([]);
    // Pagination
    const [currentPage, setCurrentPage] = useState(1);
    // Choix du nombre de podcasts par page
    const podcastsPerPage = 12;
    // Stocker l'état de l'option de tri des podcasts
    const [sortBy, setSortBy] = useState('date'); // Par défaut, on trie par date

    // Hook pour stocker les catégories de manière unique
    useEffect(() => {
        const uniqueCategories = new Set();
        datas.forEach(podcast => {
            podcast.attributes.categories.data.forEach(category => {
                uniqueCategories.add(category.attributes.nom);
            });
        });
        setCategories(uniqueCategories);
    }, [datas]);

    // Gestion de la sélection/déselection des catégories par l'utilisateur
    const handleCategoryClick = (category) => {
        if (selectedCategories.includes(category)) {
            // La catégorie est déjà sélectionnée, on la supprime
            setSelectedCategories(selectedCategories.filter((cat) => cat !== category));
        } else {
            // La catégorie n'est pas encore sélectionnée, on l'ajoute
            setSelectedCategories([...selectedCategories, category]);
        }
    };

    // Gestion du filtrage des podcasts par catégorie
    const filteredPodcasts = datas.filter((podcast) =>
        podcast.attributes.categories.data.some((category) =>
            selectedCategories.includes(category.attributes.nom)
        )
    );

    // Sélection des podcasts à afficher en fonction des filtres appliqués
    const displayedPodcasts = selectedCategories.length === 0 ? datas : filteredPodcasts;

    // Triage des podcasts en fonction de l'option de tri sélectionnée
    const sortedPodcasts = [...displayedPodcasts];

    if (sortBy === 'date') {
        // Trier par date (attribut "episode")
        sortedPodcasts.sort((a, b) => b.attributes.episode - a.attributes.episode);
    } else if (sortBy === 'ecoutes') {
        // Trier par nombre d'écoutes (attribut "nb_ecoutes")
        sortedPodcasts.sort((a, b) => b.attributes.nb_ecoutes - a.attributes.nb_ecoutes);
    }

    // Pagination des podcasts, affichage uniquement des podcasts de la page courante
    const indexOfLastPodcast = currentPage * podcastsPerPage;
    const indexOfFirstPodcast = indexOfLastPodcast - podcastsPerPage;
    const currentPodcasts = sortedPodcasts.slice(indexOfFirstPodcast, indexOfLastPodcast);

    // Calcul du nombre total de pages pour la pagination
    const totalPages = Math.ceil(displayedPodcasts.length / podcastsPerPage);

    // Fonction pour passer à la page suivante
    const nextPage = () => {
        if (currentPage < totalPages) {
            setCurrentPage(currentPage + 1);
        }
    };

    // Fonction pour passer à la page précédente
    const prevPage = () => {
        if (currentPage > 1) {
            setCurrentPage(currentPage - 1);
        }
    };

    // Configuration de l'affichage des boutons de pagination
    const maxDisplayedButtons = 5;
    const halfDisplayedButtons = Math.floor(maxDisplayedButtons / 2);
    let startPage = Math.max(1, currentPage - halfDisplayedButtons);
    let endPage = Math.min(totalPages, startPage + maxDisplayedButtons - 1);

    if (totalPages > maxDisplayedButtons) {
        if (currentPage <= halfDisplayedButtons) {
            endPage = maxDisplayedButtons;
        } else if (currentPage >= totalPages - halfDisplayedButtons) {
            startPage = totalPages - maxDisplayedButtons + 1;
            endPage = totalPages;
        }
    }

    // Création d'un tableau avec les numéros de page à afficher dans les boutons de pagination
    const pageNumbers = [];
    for (let i = startPage; i <= endPage; i++) {
        pageNumbers.push(i);
    }


    return (
        <>
            <Head>
                <title> Écoutez les Derniers Podcasts sur le Numérique | Verywell.Podcast </title>
                <meta name="description" content="Explorez notre collection de podcasts sur le numérique. Découvrez les discussions les plus récentes et informatives animées par notre équipe d'experts et d'invités spéciaux." />
                <meta property="og:title" content="Écoutez les Derniers Podcasts sur le Numérique | Verywell.Podcast" />
                <meta property="og:description" content="Explorez notre collection de podcasts sur le numérique. Découvrez les discussions les plus récentes et informatives animées par notre équipe d'experts et d'invités spéciaux." />
                <meta property="og:url" content={`${process.env.NEXT_PUBLIC_APP_URL}/podcasts`} />
                <meta property="og:type" content="website" />
                <link rel="icon" href="/img/logo.png" />
            </Head>

            {/* SECTION D'ENTETE */}
            <section className="text-white text-center relative" style={{ backgroundImage: `url(/img/podcasts/hero-podcasts.jpg)`, backgroundPosition: 'center', backgroundSize: 'cover' }}>
                <div className="absolute h-full w-full bg-zinc-900 bg-opacity-50"></div>
                <div className="relative">
                    <h1 className="font-semibold text-6xl pb-14 pt-48">Episodes</h1>

                    {/* Filtres par catégorie */}
                    <p className="text-lg">Filtrer par catégorie :</p>
                    <div className="flex overflow-x-auto md:flex-wrap md:justify-center gap-6 mx-auto px-4 py-9 lg:w-3/4 xl:w-7/12">
                        {[...categories].map((category) => (
                            <button
                                key={category}
                                className={`border opacity-80 px-4 py-2 rounded-lg  ${selectedCategories.includes(category) ? 'bg-white opacity-90 text-black' : 'hover:bg-white hover:bg-opacity-20 hover:text-white'
                                    }`}
                                onClick={() => handleCategoryClick(category)}
                            >
                                #{category}
                            </button>
                        ))}
                    </div>

                    {/* Trie par date et nombre d'écoutes */}
                    <div className="pt-2 pb-8">
                        <label className="pe-3" htmlFor="sort">Trier par: </label>
                        <select className="bg-white opacity-90 text-black border rounded p-1" value={sortBy} onChange={(e) => setSortBy(e.target.value)}>
                            <option value="date">Date</option>
                            <option value="ecoutes">Nombre d&apos;écoutes</option>
                        </select>
                    </div>

                </div>
            </section>

            {/* SECTION D'AFFICHAGE DES PODCASTS */}
            <div className="podcasts-grid">
                {currentPodcasts.map((podcast) =>

                    // Un podcast correspond à une card
                    <Card
                        categorie={
                            podcast?.attributes?.categories?.data?.map((categorie, id) =>
                                <p key={id}>{categorie.attributes.nom} &nbsp;</p>
                            )
                        }
                        date={podcast.attributes.date}
                        episode={podcast.attributes.episode}
                        id={podcast.id}
                        // nombre_commentaires='1'
                        nombre_ecoutes={podcast.attributes.nb_ecoutes}
                        // nombre_commentaires={podcast.attributes.nombre_commentaires}
                        slug={podcast.attributes.slug}
                        title={podcast.attributes.titre}
                        img={podcast.attributes.img.data.attributes.url}
                        key={podcast.id}
                    />
                )}
            </div>

            {/* PAGINATION */}
            <nav className="bg-neutral-100 py-16">
                <ul className="flex justify-center gap-2.5">
                    <li onClick={prevPage} className={`bg-slate-100 w-10 h-10 rounded flex items-center justify-center cursor-pointer ${currentPage === 1 ? 'opacity-50 pointer-events-none' : ''}`}>
                        <FiChevronLeft size={18} />
                    </li>
                    {pageNumbers.map((pageNumber) => (
                        <li key={pageNumber} onClick={() => setCurrentPage(pageNumber)} className={`bg-slate-200 w-10 h-10 rounded flex items-center justify-center cursor-pointer hover:border hover:border-neutral-300 ${pageNumber === currentPage ? "text-white bg-primary" : ""}`} style={{ backgroundImage: pageNumber === currentPage ? "var(--linear-primary)" : "" }}>
                            {pageNumber}
                        </li>
                    ))}
                    <li onClick={nextPage} className={` w-10 h-10 rounded flex items-center justify-center cursor-pointer ${currentPage === totalPages ? 'opacity-50 pointer-events-none' : ''}`}>
                        <FiChevronRight size={18} />
                    </li>
                </ul>
            </nav>
        </>
    );
}