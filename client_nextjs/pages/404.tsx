import Link from "next/link";

export default function Custom404() {

    return (
        <>
            <div className="h-screen relative text-white">
                <div id="erreur-404" className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 pb-12 text-center">

                    {/* 404 */}
                    <svg className="mx-auto" width="228" height="127" viewBox="0 0 228 127" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M38.1 124V96.7H0.75V78.25L32.4 2.49995H61.5V78.0999H72.75V96.7H61.5V124H38.1ZM19.8 78.0999H38.1V27.5499L19.8 78.0999Z" stroke="white" stroke-width="3" />
                        <path d="M113.912 126.25C106.612 126.25 100.512 124.8 95.6121 121.9C90.7121 118.9 87.0121 114.8 84.5121 109.6C82.0121 104.4 80.7621 98.35 80.7621 91.45V36.3999C80.7621 29.3 81.9121 23.1 84.2121 17.8C86.6121 12.4 90.2621 8.19995 95.1621 5.19995C100.062 2.19995 106.312 0.699951 113.912 0.699951C121.512 0.699951 127.712 2.19995 132.512 5.19995C137.412 8.19995 141.012 12.4 143.312 17.8C145.712 23.1 146.912 29.3 146.912 36.3999V91.45C146.912 98.25 145.662 104.3 143.162 109.6C140.662 114.8 136.962 118.9 132.062 121.9C127.162 124.8 121.112 126.25 113.912 126.25ZM113.912 103.9C116.312 103.9 118.012 103.15 119.012 101.65C120.112 100.05 120.762 98.25 120.962 96.25C121.262 94.15 121.412 92.3499 121.412 90.8499V36.9999C121.412 35.3 121.312 33.4 121.112 31.2999C120.912 29.0999 120.262 27.1999 119.162 25.6C118.162 23.9 116.412 23.0499 113.912 23.0499C111.412 23.0499 109.612 23.9 108.512 25.6C107.512 27.1999 106.912 29.0999 106.712 31.2999C106.512 33.4 106.412 35.3 106.412 36.9999V90.8499C106.412 92.3499 106.562 94.15 106.862 96.25C107.162 98.25 107.812 100.05 108.812 101.65C109.912 103.15 111.612 103.9 113.912 103.9Z" stroke="white" stroke-width="3" />
                        <path d="M193.233 124V96.7H155.883V78.25L187.533 2.49995H216.633V78.0999H227.883V96.7H216.633V124H193.233ZM174.933 78.0999H193.233V27.5499L174.933 78.0999Z" stroke="white" stroke-width="3" />
                    </svg>

                    {/* Texte */}
                    <h1 className="text-2xl pt-5">Oups ! Cette page reste introuvable...</h1>
                    <p className="text-sm pt-10 pb-4">Revenez à l&apos;accueil ci-dessous ou reprenez la navigation depuis le menu.</p>

                    {/* Bouton */}
                    <Link href="/">
                        <button className="button-hover2 capitalize border border-neutral-500 rounded-full px-8 py-4 bg-white zinc-900 bg-opacity-10 hover:bg-opacity-30">Revenir à l&apos;accueil</button>
                    </Link>
                    
                </div>
            </div>
        </>
    )
}