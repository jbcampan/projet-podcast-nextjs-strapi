import '@/styles/globals.css'
import ApiProvider from "../context/apiContext"
import Layout from '../components/layout'
import type { AppProps } from 'next/app'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ApiProvider>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ApiProvider>
  )
}
