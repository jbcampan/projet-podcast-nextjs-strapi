import Head from 'next/head'
import Link from 'next/link';
import Collaborate from "@/components/Collaborate";
import NewsLetter from "@/components/Newsletter";
import Team from "@/components/Team";

export default function A_Propos() {

    return (
        <>
            <Head>
                <title> À Propos de Verywell.Podcast </title>
                <meta name="description" content="Découvrez l'histoire et la mission de Verywell.Podcast. Apprenez-en plus sur notre équipe passionnée et sur la manière dont nous apportons les meilleures discussions sur le numérique à vos oreilles." />
                <link rel="icon" href="/img/logo.png" />
            </Head>

            {/* ENTETE */}
            <section className="text-white">
                <div className="pt-40 pb-20 text-center font-bold">
                    <h1 className="outline-text text-5xl sm:text-7xl pb-4">Verywell.Podcast</h1>
                    <h2 className="text-3xl sm:text-5xl">Découvrez-nous</h2>
                </div>
            </section>

            {/* SECTION PRESENTATION ENTREPRISE */}
            <section className="bg-neutral-100 py-20 sm:pt-28 sm:pb-28">
                <div className="px-4 sm:px-0 sm:w-4/5 mx-auto sm:flex justify-between items-center">
                    <div className="lg:w-3/5">
                        <p className="barre relative uppercase text-neutral-400"><span className="ms-7">A propos de nous</span></p>
                        <h2 className="text-2xl uppercase font-semibold pb-4">Notre engagement</h2>
                        <h3 className="text-4xl sm:text-5xl font-semibold py-8"><span className="text-gradient">Nous partageons</span> notre passion du numérique <span className="text-gradient">avec vous</span></h3>
                        <p className="text-neutral-500 pt-5 pb-8">Ut at egestas erat. Duis quis accumsan eros. Nunc lobortis, justo sit amet posuere pharetra, tortor ante convallis elit, vel faucibus urna massa a lectus. Duis orci tellus, condimentum sed tellus a, condimentum efficitur eros. Nulla porta egestas consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam eu lorem hendrerit erat vulputate gravida ut eu nisi. <br /><br /> Praesent eros lectus, accumsan porttitor interdum non, faucibus id libero. Mauris ultricies ultrices volutpat. Phasellus dapibus sem nunc, at egestas tellus mattis ac. Vestibulum sit amet vehicula sapien. Suspendisse placerat consectetur auctor. <br /><br /> Morbi feugiat nisi sit amet est iaculis, vitae fringilla nisl laoreet. Maecenas in risus dui.</p>
                        <Link href="/podcasts" className="button-hover text-white font-bold px-8 py-4 sm:px-12 sm:py-6 rounded-full inline-flex justify-center items-center " style={{ backgroundImage: "var(--linear-primary)" }}>Ecoutez nos Podcasts</Link>
                    </div>
                    <div className="py-4 md:py-0 md:w-1/3">
                        <img className=" sm:hidden lg:block rounded-xl" src="/img/accueil/nous.jpg" alt="" />
                    </div>
                </div>
            </section>

            {/* SECTION PRESENTATION EQUIPE */}
            <section className="bg-gris-F1 py-20 sm:pt-14 sm:pb-24">
                <div className="px-4 sm:px-0 sm:w-4/5 mx-auto">
                    <div className="lg:w-3/5">
                        <p className="barre relative uppercase text-neutral-400"><span className="ms-7">Qui nous sommes</span></p>
                        <h2 className="text-2xl uppercase font-semibold pb-16">Notre équipe</h2>
                    </div>

                    <div className="team-grid gap-6">
                        <Team name="Prenom Nom" metier="Metier" img="team1.jpg" description="Descritpion -  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed leo diam, tristique eget egestas eget, eleifend porta orci. Mauris tempus nisl eleifend mi laoreet euismod. Pellentesque ut rhoncus leo, ullamcorper finibus velit." />
                        <Team name="Prenom Nom" metier="Metier" img="team2.png" description="Descritpion -  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed leo diam, tristique eget egestas eget, eleifend porta orci. Mauris tempus nisl eleifend mi laoreet euismod. Pellentesque ut rhoncus leo, ullamcorper finibus velit." />
                        <Team name="Prenom Nom" metier="Metier" img="team3.jpg" description="Descritpion -  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed leo diam, tristique eget egestas eget, eleifend porta orci. Mauris tempus nisl eleifend mi laoreet euismod. Pellentesque ut rhoncus leo, ullamcorper finibus velit." />
                    </div>
                </div>
            </section>

            {/* SECTION COLLABORER */}
            <Collaborate />

            {/* SECTION NEWSLETTER */}
            <NewsLetter />
        </>
    )
}