import React, { useState, useRef, useEffect } from 'react'
import styles from './AudioPlayer.module.css';

import { BsArrowLeftShort } from 'react-icons/bs';
import { BsArrowRightShort } from 'react-icons/bs';
import { FaPlay } from 'react-icons/fa';
import { FaPause } from 'react-icons/fa';

const AudioPlayer = ({ timeJump, podcastData }) => {

  //Etat de lecture du lecteur audio
  const [isPlaying, setIsPlaying] = useState(false);
  // Durée totale de l'audio en secondes
  const [duration, setDuration] = useState(0);
  // Temps actuel de l'audio en secondes
  const [currentTime, setCurrentTime] = useState(0);
  // Incrémentation du temps d'écoute en secondes
  const [time, setTime] = useState(0);
  // Statut de l'incrémentation du temps
  const [isRunning, setIsRunning] = useState(false);

  //Références pour accéder aux éléments du DOM
  const audioPlayer = useRef(); //reference au composant audio 
  const progressBar = useRef(); //reference à la progress bar
  const animationRef = useRef(); //reference à l'animation

  // Gérer les sauts dans le temps dans l'audio en fonction de la propriété timeJump
  useEffect(() => {
    if (timeJump) {
      timeTravel(timeJump);
      setIsPlaying(true);
      play();
    } else {
      timeTravel(0);
    }
  }, [timeJump])

  useEffect(() => {
    // Mettre à jour la durée et la valeur maximale de la barre de progression lorsque les métadonnées de l'audio sont chargées
    const updateDuration = () => {
      const seconds = Math.floor(audioPlayer.current.duration);
      setDuration(seconds);
      progressBar.current.max = seconds;
    };

    if (audioPlayer.current && audioPlayer.current.readyState >= 2) {
      // Les métadonnées de l'audio sont déjà chargées, donc on peut mettre à jour la durée directement
      updateDuration();
    } else {
      // Les métadonnées de l'audio ne sont pas encore chargées, on ajoute un écouteur d'événement pour détecter quand elles seront prêtes
      audioPlayer.current.addEventListener('loadedmetadata', updateDuration);
    }

    // Nettoyage de l'écouteur d'événement lorsque le composant est démonté pour éviter les fuites de mémoire
    return () => {
      if (audioPlayer.current) {
        audioPlayer.current.removeEventListener('loadedmetadata', updateDuration);
      }
    };
  }, []);

  // Mettre à jour la durée et la valeur maximale de la barre de progression lorsque le podcast est chargé
  useEffect(() => {
    const seconds = Math.floor(audioPlayer.current.duration);
    setDuration(seconds);
    progressBar.current.max = seconds;
  }, [audioPlayer?.current?.loadedmetadata, audioPlayer?.current?.readyState])

  // Fonction pour afficher le temps au format minutes:secondes
  const calculateTime = (secs) => {
    const minutes = Math.floor(secs / 60);
    const returnedMinutes = minutes < 10 ? `0${minutes}` : `${minutes}`;
    const seconds = Math.floor(secs % 60);
    const returnedSeconds = seconds < 10 ? `0${seconds}` : `${seconds}`;
    return `${returnedMinutes} : ${returnedSeconds}`;
  }

  // Fonction pour lancer la lecture de l'audio et mettre à jour la barre de progression
  const play = () => {
    audioPlayer.current.play();
    animationRef.current = requestAnimationFrame(whilePlaying)
  }

  // Fonction pour gérer la lecture/pause de l'audio et arrêter l'animation
  const togglePlayPause = () => {
    const prevValue = isPlaying;
    setIsPlaying(!prevValue);

    if (!prevValue) {
      play();
    } else {
      audioPlayer.current.pause();
      cancelAnimationFrame(animationRef.current);
    }
  }

  // Fonction exécutée à chaque frame pour mettre à jour la barre de progression pendant la lecture
  const whilePlaying = () => {
    if (audioPlayer.current && progressBar.current) {
      progressBar.current.value = audioPlayer.current.currentTime;
      changePlayerCurrentTime();
      animationRef.current = requestAnimationFrame(whilePlaying);
    }
  };

  // Gérer l'animation de la barre de progression en fonction du statut de lecture
  useEffect(() => {
    if (isPlaying) {
      animationRef.current = requestAnimationFrame(whilePlaying);
    }

    return () => {
      cancelAnimationFrame(animationRef.current);
    };
  }, [isPlaying]);


  // Fonction pour mettre à jour la position de lecture en fonction de la valeur de la barre de progression
  const changeRange = () => {
    audioPlayer.current.currentTime = progressBar.current.value;
    changePlayerCurrentTime();
  }

  // Fonction pour mettre à jour le style de la barre de progression et le temps actuel du lecteur
  const changePlayerCurrentTime = () => {
    progressBar.current.style.setProperty('--seek-before-width', `${progressBar.current.value / duration * 100}%`)
    setCurrentTime(progressBar.current.value);
  }

  // Fonction pour effectuer un saut en avant ou en arrière de 30 secondes
  const backThirty = () => {
    timeTravel(Number(progressBar.current.value) - 30);
  }

  const forwardThirty = () => {
    timeTravel(Number(progressBar.current.value) + 30);
  }

  // Fonction pour effectuer un saut temporel dans l'audio en fonction du nouveau temps
  const timeTravel = (newTime) => {
    progressBar.current.value = newTime;
    changeRange();
  }


  //SECTION INCREMENTATION

  // Gérer l'incrémentation du temps d'écoute en secondes
  useEffect(() => {
    let intervalId;
    if (isRunning) {
      // setting time from 0 to 1 every 1 seconde using javascript setInterval method
      intervalId = setInterval(() => setTime((prevTime) => prevTime + 1), 1000);
    }
    return () => clearInterval(intervalId);
  }, [isRunning]);

  // Calcul des minutes et secondes pour l'incrémentation du temps d'écoute
  const minutes = Math.floor(time / 60);
  const seconds = time % 60;

  // Fonction pour démarrer et arrêter le timer d'incrémentation du temps d'écoute
  const startAndStop = () => {
    setIsRunning((prevIsRunning) => !prevIsRunning);
  };

  // Fonction pour démarrer/arrêter la lecture et le timer d'incrémentation
  const togglePlayPauseAndStartStop = () => {
    togglePlayPause();
    startAndStop();
  };

  //UPDATE NB_ECOUTES

  // Récupération de l'identifiant du podcast et du nombre d'écoutes actuel
  const podcastId = podcastData.id;
  const nbEcoutes = podcastData.nb_ecoutes;

  // Fonction pour mettre à jour le nombre d'écoutes du podcast avec requête PUT
  const updatePodcastListens = async (podcastId) => {
    try {
      const requestBody = {
        data: {
          nb_ecoutes: nbEcoutes + 1, //Incrémenter le nombre d'écoute de 1
        },
      };

      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/podcasts/${podcastId}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody),
      });

      if (response.status === 200) {
        console.log('Podcast listens updated successfully.');
      } else {
        console.error('Failed to update podcast listens:', response.statusText);
      }
    } catch (error) {
      console.error('Error updating podcast listens:', error);
    }
  };

  // Appel de la fonction updatePodcastListens lorsqu'on atteint le temps mentionné
  useEffect(() => {
    if (minutes === 0 && seconds === 10) {
      console.log(`nous avons atteint les ${minutes} minutes et ${seconds} secondes`);
      updatePodcastListens(podcastId); // Appeler la fonction pour incrémenter le nombre d'écoutes du podcast
    }
  }, [minutes, seconds, podcastId]);

  return (
    <div className={`${styles.audioPlayer} flex flex-col items-center gap-3 sm:flex sm:flex-row sm:items-center`}>
      <audio ref={audioPlayer} src={`${process.env.NEXT_PUBLIC_API_URL}${podcastData.mp3?.url}`} preload="metadata"></audio>
      <div className='flex items-center'>
        {/* Revenir en arrière */}
        <button className={styles.forwardBackward} onClick={backThirty}><BsArrowLeftShort /> 30</button>

        {/* Bouton lecteur Start / Pause */}
        <button onClick={togglePlayPauseAndStartStop} className={styles.playPause}>
          {isPlaying ? <FaPause /> : <FaPlay className={styles.play} />}
        </button>

        {/* Aller en avant */}
        <button className={styles.forwardBackward} onClick={forwardThirty}>30 <BsArrowRightShort /></button>
      </div>

      <div className='flex items-center'>

        {/* temps actuel */}
        <div className={styles.currentTime}>{calculateTime(currentTime)}</div>

        {/* barre de progression */}
        <div>
          <input type="range" className={styles.progressBar} defaultValue="0" ref={progressBar} onChange={changeRange} />
        </div>

        {/* durée */}
        <div className={styles.duration}>
          {typeof duration === 'number' && !isNaN(duration) ? calculateTime(duration) : ''}
        </div>
      </div>

    </div>
  )
}

export default AudioPlayer
