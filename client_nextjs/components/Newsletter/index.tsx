import Link from "next/link";
import { BiLogoFacebook } from 'react-icons/bi';
import { BiLogoInstagram } from 'react-icons/bi';
import { BiLogoLinkedin } from 'react-icons/bi';
import { BiLogoSoundcloud } from 'react-icons/bi';
import { BiLogoSpotify } from 'react-icons/bi';
import { BiLogoTwitter } from 'react-icons/bi';
import { PiEnvelopeSimpleOpenThin } from 'react-icons/pi';

// Définition du composant Newsletter qui permet de s'inscrire à la Newsletter
export default function NewsLetter() {

  return (
    <section className="flex flex-col items-center text-center py-14 bg-neutral-100	px-2">

      {/* Logo */}
      <div className="rounded-full" style={{ backgroundImage: "var(--linear-primary)" }}>
        <PiEnvelopeSimpleOpenThin size={100} className="border rounded-full p-4 shadow-md text-white" />
      </div>

      {/* Titre et sous-titre */}
      <h2 className="text-3xl sm:text-4xl font-bold py-5">Abonnez-Vous à notre <br /> newsletter</h2>
      <p className="text-neutral-500">Soyez alertés de la sortie de nos derniers épisodes de podcasts <br /> directement dans votre boîte mail !</p>

      {/* Formulaire */}
      <form action="">
        <div className="flex flex-col sm:flex-row py-9 gap-y-3">
          <div>
            <input className="p-3 pe-20 border rounded-xl sm:rounded-e-none bg-neutral-100 border-neutral-400" type="text" placeholder="Votre prénom" />
          </div>
          <div>
            <input className="p-3 pe-20 border rounded-xl sm:rounded-s-none bg-neutral-100 border-neutral-400	" type="text" placeholder="Votre adresse mail" />
          </div>
        </div>
        <button className="button-hover px-8 py-4 rounded-full text-white font-bold" style={{ backgroundImage: "var(--linear-primary)" }}>S&apos;abonner</button>
      </form>

      {/* Réseaux sociaux */}
      <ul className="flex gap-6 pt-8 lg:pt-16 mt-2">
        <li>
          <Link href=""><BiLogoTwitter size={35} className="button-hover border rounded-full p-1 border-neutral-400 hover:bg-neutral-50" /></Link>
        </li>
        <li>
          <Link href=""><BiLogoFacebook size={35} className="button-hover border rounded-full p-1 border-neutral-400 hover:bg-neutral-50" /></Link>
        </li>
        <li>
          <Link href=""><BiLogoInstagram size={35} className="button-hover border rounded-full p-1 border-neutral-400 hover:bg-neutral-50" /></Link>
        </li>
        <li>
          <Link href=""><BiLogoLinkedin size={35} className="button-hover border rounded-full p-1 border-neutral-400 hover:bg-neutral-50" /></Link>
        </li>
        <li>
          <Link href=""><BiLogoSoundcloud size={35} className="button-hover border rounded-full p-1 border-neutral-400 hover:bg-neutral-50" /></Link>
        </li>
        <li>
          <Link href=""><BiLogoSpotify size={35} className="button-hover border rounded-full p-1 border-neutral-400 hover:bg-neutral-50" /></Link>
        </li>
      </ul>

    </section>
  );
}

