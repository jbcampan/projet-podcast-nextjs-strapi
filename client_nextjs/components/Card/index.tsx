import Link from "next/link";
import { PiChats } from 'react-icons/pi';
import { PiHeadphonesDuotone } from 'react-icons/pi';
import { BsFillPlayCircleFill } from 'react-icons/bs';
import styles from "./Card.module.css";


// Définition des propriétés que le composant Card peut recevoir
interface CardProps {
    categorie: React.ReactNode[];
    date: string;
    episode: number;
    id?: string;
    img?: string;
    nombre_commentaires?: string;
    nombre_ecoutes?: number;
    slug: string;
    title: string;
}

// Définition du composant Card qui permet d'afficher les podcasts dans la page de liste des podcasts
const Card: React.FC<CardProps> = ({
    categorie,
    date,
    episode,
    id,
    img,
    nombre_commentaires,
    nombre_ecoutes,
    slug,
    title,
}) => {

    return (

        // Lien vers le podcast sélectionné
        <Link href={`/podcasts/${slug}`} className={`LinkHover text-white text-xs uppercase relative  ${styles.squareCard}`} style={{ backgroundImage: `url(${process.env.NEXT_PUBLIC_API_URL}${img})`, backgroundPosition: 'center', backgroundSize: 'cover' }}>

            {/* Ecran grisé */}
            <div className="hover-effect absolute h-full w-full bg-zinc-900 bg-opacity-50"></div> 

            {/* Bouton Play */}
            <BsFillPlayCircleFill size={80} className="playButton absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 opacity-70" />

            {/* Infos Podcast */}
            <div className="LinkHover p-7 absolute bottom-0">
                <div className="flex">
                    {categorie}
                </div>
                <h5 className="text-3xl capitalize py-3">{title}</h5>
                <div className="flex pb-2">
                    <p className=""> Episode #{episode}</p>
                    <p>&nbsp; - &nbsp;</p>
                    <p>{date}</p>
                </div>
                <div className="flex items-center">
                    {/* Si on souhaite afficher le nombre de commentaires du podcast, à décommenter (pas encore relié à Disqus) */}
                    {/* <div className="flex items-center pe-4">
                        <PiChats size={25} className="pe-1" />
                        <p>{nombre_commentaires}</p>
                    </div> */}
                    <div className="flex items-center">
                        <PiHeadphonesDuotone size={25} className="pe-1" />
                        <p>{nombre_ecoutes}</p>
                    </div>
                </div>
            </div>

        </Link>

    )
}

export default Card;