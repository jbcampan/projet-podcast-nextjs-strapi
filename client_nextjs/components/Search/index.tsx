import { useState, useEffect } from 'react';
import Link from "next/link";
import { useApi } from '../../context/apiContext';
import { AiOutlineSearch } from 'react-icons/ai';
import { AiOutlineClose } from 'react-icons/ai';
import styles from '@/components/Search/Search.module.css';

// Définition du composant Search pour permettre de faire une recherche par titre de podcast
export default function Search({closeNav}:any) {

    // Stocker l'état d'ouverture ou de fermeture de la fenêtre modale de recherche
    const [isSearchOpen, setSearchOpen] = useState(false);
    // Récupérer les données de ApiContext
    const { datas } = useApi();
    // Stocker le texte de recherche saisi par l'utilisateur
    const [searchText, setSearchText] = useState('');
    
    // Fonction pour gérer l'ouverture de la fenêtre modale
    const handleSearch = () => {
        setSearchOpen(!isSearchOpen);
    }

    // Fonction pour fermer la fenêtre modale, et le menu de navigation lorsqu'on choisit un podcast dans la liste de recherche
    const closeSearch = () => {
        setSearchOpen(false);
        closeNav();
    }

    // Fonction pour gérer les changements dans le champ de recherche
    const handleSubject = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchText(event.target.value);
    }

    // Filtrer les podcasts en fonction du texte de recherche saisi
    const filteredPodcasts = datas.filter(podcast => podcast.attributes.titre.toLowerCase().includes(searchText.toLowerCase()))

    return (
        <>
        {/* Bouton Recherche dans la barre de menu */}
            <button className="button-hover2 p-3 border rounded-full hover:bg-white hover:bg-opacity-20 transition"><AiOutlineSearch size={15} onClick={handleSearch} /></button>

            {/* FENETRE MODALE DE RECHERCHER */}
            {isSearchOpen && (
            <div className="fixed top-0 left-0 w-full h-full" style={{ backgroundColor: "var(--dark-grey-custom)" }}>
                <div className="w-3/4 mx-auto relative pt-6">

                    {/* ENTETE */}
                    <div className="flex items-center gap-4 pb-10">
                        <button className="p-3 border rounded-full cursor-default"><AiOutlineSearch size={15} /></button>

                        {/* Champs de recherche */}
                        <input className='bg-transparent w-full p-2' onChange={handleSubject} value={searchText} placeholder='Rechercher un sujet...'></input>

                        {/* Fermeture du menu de recherche */}
                        <button className='' onClick={handleSearch}><AiOutlineClose size={30} /></button>

                    </div>

                    {/* RESULTATS DE RECHERCHE */}
                    <div className={`w-full lg:w-3/4 mx-auto pt-20 overflow-y-auto max-h-80vh ${styles.podcastSearch}`}>
                        {/* si le champs de recherche est vide on affiche les "Commencez à écrire, sinon on affiche les résultats */}
                        {searchText == '' ?
                        <p className={`text-4xl ${styles.scrollTitle}`}>Commencez à écrire...</p>
                        :
                        <div className='flex flex-col gap-6 w-full sm:w-3/4'>
                            <h1 className='uppercase font-semibold'>Episodes</h1>
                            {filteredPodcasts.map((podcast) =>
                                <Link href={`${process.env.NEXT_PUBLIC_APP_URL}/podcasts/${podcast.attributes.slug}`} key={podcast.id}>
                                    <div className='button-hover ps-2 py-4 bg-white bg-opacity-10 rounded' onClick={closeSearch}>
                                        <p>Episode {podcast.attributes.episode} - {podcast.attributes.titre}</p>
                                    </div>
                                </Link>
                            )}
                        </div>
                        }
                    </div>

                </div>
            </div>
            )}  
        </>
    )
}