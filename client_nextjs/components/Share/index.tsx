import React from "react"

import { EmailIcon, FacebookIcon, LinkedinIcon, PinterestIcon, TwitterIcon } from "react-share";
import { EmailShareButton, FacebookShareButton, LinkedinShareButton, PinterestShareButton, TwitterShareButton } from "react-share";

// Définition des propriétés attendues par le composant
interface ShareProps {
    description: string,
    podcastSlug: any
}

// Définition du composant Share qui permet de partager un podcast sur les réseaux sociaux (grâce aux composants de react-share)
function Share({ description, podcastSlug }: ShareProps) {
    const url = `${process.env.NEXT_PUBLIC_APP_URL}/podcasts/${podcastSlug}`

    const imageUrl = "URL_DE_L_IMAGE"; // Remplacer par l'URL de l'image à partager sur Pinterest

    return (
        <>
            <p className="text-xs uppercase block lg:hidden">Partager</p>
            <div className="podcast-social flex lg:justify-end" >
                <ul className="flex lg:flex-col gap-3 pe-12 pb-6 lg:pt-16 mt-2">
                    <li>
                        <TwitterShareButton url={`${url}`}>
                            <TwitterIcon iconFillColor="black" bgStyle={{ fill: 'transparent' }} size={35} round={true} className="button-hover border rounded-full hover:bg-neutral-50" target="_blank"></TwitterIcon>
                        </TwitterShareButton>
                    </li>
                    <li>
                        <FacebookShareButton url={`${url}`}>
                            <FacebookIcon iconFillColor="black" bgStyle={{ fill: 'transparent' }} size={35} round={true} className="button-hover border rounded-full hover:bg-neutral-50" target="_blank"></FacebookIcon>
                        </FacebookShareButton>
                    </li>
                    <li>
                        <LinkedinShareButton url={`${url}`}>
                            <LinkedinIcon iconFillColor="black" bgStyle={{ fill: 'transparent' }} size={35} round={true} className="button-hover border rounded-full hover:bg-neutral-50" target="_blank"></LinkedinIcon>
                        </LinkedinShareButton>
                    </li>
                    <li>
                        <PinterestShareButton url={`${url}`} media={imageUrl}>
                            <PinterestIcon iconFillColor="black" bgStyle={{ fill: 'transparent' }} size={35} round={true} className="button-hover border rounded-full hover:bg-neutral-50" target="_blank"></PinterestIcon>
                        </PinterestShareButton>
                    </li>
                    <li>
                        <EmailShareButton url={`${url}`}>
                            <EmailIcon iconFillColor="black" bgStyle={{ fill: 'transparent' }} size={35} round={true} className="button-hover border rounded-full hover:bg-neutral-50" target="_blank"></EmailIcon>
                        </EmailShareButton>
                    </li>
                    <li>
                        <p className="text-xl uppercase rotate-180 pe-7 hidden lg:block" style={{ writingMode: 'vertical-rl' }}>Partager</p>
                    </li>
                </ul>
            </div>
        </>
    )
}

export default Share;