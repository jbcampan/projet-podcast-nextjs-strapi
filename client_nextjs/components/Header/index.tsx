import Link from "next/link";
import { useState } from "react";
import Search from '../Search';
import styles from '@/components/Header/Header.module.css';
import { RxHamburgerMenu } from 'react-icons/rx';

export default function Header() {

    // Gérer l'état d'ouverture/fermeture de la navigation mobile (fenêtre modale)
    const [isNavOpen, setIsNavOpen] = useState(false);

    // Ouvrir ou fermer la navigation mobile
    const toggleNav = () => {
        setIsNavOpen(!isNavOpen);
    };

    // Fermer la navigation mobile quand on choisit un podcast dans le composant Search
    const closeNav = () => {
        setIsNavOpen(false);
    };

    return (
        <nav className={`text-white text-xl ${isNavOpen ? "bg-opacity-50" : "bg-transparent"} absolute top-0 left-0 w-full ${styles.headerIndex}`}>

            {/* Header format Desktop */}
            <div className="hidden md:flex justify-around p-5 items-center">
                <Link href="/" className="flex items-center">
                    <img src="/img/header/logo.png" className="h-12 mr-3" alt="Verywell Podcast Logo" />
                </Link>
                <div className="">
                    <ul className="flex gap-x-10">
                        <li>
                            <Link href="/" className="menu-link" onClick={closeNav}> Accueil </Link>
                        </li>
                        <li>
                            <Link href="/podcasts" className="menu-link" onClick={closeNav}> Podcasts </Link>
                        </li>
                        <li>
                            <Link href="/a-propos" className="menu-link" onClick={closeNav}> A Propos </Link>
                        </li>
                        <li>
                            <Link href="/contact" className="menu-link" onClick={closeNav}> Contact </Link>
                        </li>
                    </ul>
                </div>
                <div className="flex items-center gap-8">
                    <Search closeNav={closeNav} />
                    {/* <div className="border rounded-full p-2  bg-white bg-opacity-10 text-base">Se Connecter</div> */}
                </div>
            </div>

            {/* Header format Mobile */}
            <div className="md:hidden flex justify-between p-5 items-center">
                <Link href="/" className="flex items-center">
                    <img src="/img/header/logo.png" className="h-12 mr-3" alt="Verywell Podcast Logo" />
                </Link>
                <div className="md:hidden relative z-10">
                    <button onClick={toggleNav}>
                        <RxHamburgerMenu size={20} />
                    </button>
                </div>
            </div>

            {/* Navigation format Mobile (fenêtre modale) */}
            <div className={`md:hidden fixed top-0 left-0 w-full h-full flex items-center justify-center translate-x-full transition ${isNavOpen ? "translate0" : "translate-x-full"}`} style={{ backgroundColor: "var(--dark-grey-custom)" }}>
                <ul className="flex flex-col gap-y-5 mt-5">
                    <li>
                        <Link href="/" className="" onClick={closeNav}> Accueil </Link>
                    </li>
                    <li>
                        <Link href="/podcasts" className="" onClick={closeNav}> Podcasts </Link>
                    </li>
                    <li>
                        <Link href="/a-propos" className="" onClick={closeNav}> A Propos </Link>
                    </li>
                    <li>
                        <Link href="/contact" className="" onClick={closeNav}> Contact </Link>
                    </li>
                    <li >
                        <div className="flex flex-col items-start gap-y-5">
                            <Search closeNav={closeNav} />
                            {/* <div onClick={closeNav} className="border rounded-full p-2  bg-white bg-opacity-10 text-base">Se Connecter</div> */}
                        </div>
                    </li>
                </ul>
            </div>

        </nav>
    );
}

