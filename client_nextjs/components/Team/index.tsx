// Définition des propriétés attendues par le composant
interface TeamProps {
    description: string;
    img: string;
    metier: string;
    name: string;

}

// Définition du composant Team qui représente les différents membres de l'équipe page A Propos
const Team: React.FC<TeamProps> = ({
    description,
    img,
    metier,
    name

}) => {
    return (

        <div>
            <img className="h-56 w-full object-cover" src={`/img/a_propos/${img}`} alt="" />
            <div className="border-card-team bg-neutral-200 p-5">
                <p className="uppercase text-2xl pb-4">{name}</p>
                <p className="uppercase text-lg pb-4">{metier}</p>
                <p className="text-sm font-light">{description}</p>
            </div>
        </div>
    );
}

export default Team;