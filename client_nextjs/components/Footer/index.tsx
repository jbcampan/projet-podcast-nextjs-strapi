import Link from "next/link";

import { BiLogoFacebook } from 'react-icons/bi';
import { BiLogoInstagram } from 'react-icons/bi';
import { BiLogoLinkedin } from 'react-icons/bi';
import { BiLogoSoundcloud } from 'react-icons/bi';
import { BiLogoSpotify } from 'react-icons/bi';
import { BiLogoTwitter } from 'react-icons/bi';
import styles from "./Footer.module.css";


export default function Footer() {

    return (

        <footer className={`text-white text-xs justify-around py-4 lg:py-20 ${styles.footerGrid}`}>

            {/* Logo + Titre / mentions légales */}
            <div>
                <div className="flex items-center pb-6">
                    <img className="h-12" src="/img/header/logo.png" alt="" />
                    <p className="text-2xl">Verywell.Podcast</p>
                </div>
                <div>
                    <Link href=""><p className="p-1">Mentions légales</p></Link>
                    <Link href=""><p className="p-1">Politique de confidentialité</p></Link>
                </div>
            </div>

            {/* Newsletter */}
            <div>
                <p className="uppercase text-2xl font-bold pb-6">Newsletter</p>
                <div>
                    <p className="p-3">Restez informés</p>
                    <div>
                        <input className="px-8 py-4 rounded-full" type="text" placeholder="adressemail@mail.com" />
                    </div>
                </div>
            </div>

            {/* Navigation */}
            <ul className="flex flex-col justify-between">
                <li className="py-3">
                    <Link href="/" className="hover:border-b">Accueil</Link>
                </li>
                <li className="py-3">
                    <Link href="/podcasts" className="hover:border-b">Podcasts</Link>
                </li>
                <li className="py-3">
                    <Link href="/a-propos" className="hover:border-b">A Propos</Link>
                </li>
                <li className="py-3">
                    <Link href="/contact" className="hover:border-b">Contact</Link>
                </li>
            </ul>

            {/* Réseaux Sociaux */}
            <ul className="grid grid-cols-3 lg:grid-cols-2 gap-6 md:gap-0 lg:gap-6">
                <li>
                    <Link href=""><BiLogoTwitter size={35} className="border rounded-full p-1 hover:bg-white hover:bg-opacity-10 transition button-hover2" /></Link>
                </li>
                <li>
                    <Link href=""><BiLogoFacebook size={35} className="border rounded-full p-1 hover:bg-white hover:bg-opacity-10 transition button-hover2" /></Link>
                </li>
                <li>
                    <Link href=""><BiLogoInstagram size={35} className="border rounded-full p-1 hover:bg-white hover:bg-opacity-10 transition button-hover2" /></Link>
                </li>
                <li>
                    <Link href=""><BiLogoLinkedin size={35} className="border rounded-full p-1 hover:bg-white hover:bg-opacity-10 transition button-hover2" /></Link>
                </li>
                <li>
                    <Link href=""><BiLogoSoundcloud size={35} className="border rounded-full p-1 hover:bg-white hover:bg-opacity-10 transition button-hover2" /></Link>
                </li>
                <li>
                    <Link href=""><BiLogoSpotify size={35} className="border rounded-full p-1 hover:bg-white hover:bg-opacity-10 transition button-hover2" /></Link>
                </li>
            </ul>

        </footer>

    )
}