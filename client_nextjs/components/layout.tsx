import Header from './Header'
import Footer from './Footer'

// Définition du composant Layout qui permet d'englober le contenu entre les composants Header et Footer
export default function Layout({ children }:any) {
    return (
        <>
            <Header />
            <main>{children}</main>
            <Footer />
        </>
    )
}