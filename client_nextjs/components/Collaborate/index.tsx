import Link from "next/link";
import { FaRegHandshake } from 'react-icons/fa';

// Définition du composant Collaborate qui renvoit sur le formulaire de contact
export default function Collaborate() {

  return (
    <section className="relative mx-auto flex justify-center items-center py-28 text-white" style={{ backgroundImage: `url(/img/accueil/collaborer.jpg)`, backgroundPosition: 'center', backgroundSize: 'cover' }}>
      <div className="absolute h-full w-full bg-zinc-900 bg-opacity-50"></div>
      <FaRegHandshake size={200} className="hidden lg:block relative w-1/3" />
      <div className="relative px-4 sm:px-0 sm:w-3/4 lg:w-1/3">
        <p className="barre relative uppercase"><span className="ms-7">Collaborer</span></p>
        <h2 className="text-2xl uppercase font-semibold pb-4">Nous vous recevons</h2>
        <h3 className="text-4xl sm:text-5xl font-semibold py-3">Une expertise <br /> à <span className="text-gradient">partager</span> ?</h3>
        <p className="text-2xl py-7">Contactez-nous et nous aurons le plaisir de vous recevoir pour participer avec nous !</p>
        <Link href="/contact">
          <button className="button-hover2 capitalize border border-neutral-500 rounded-full px-8 py-4 bg-white zinc-900 bg-opacity-20">Contactez-nous</button>
        </Link>
      </div>
    </section>
  );
}

