
#   Podcast.Verywell.Digital - Plateforme de Podcasts

![Page d'Accueil du site](design/accueil.png)

&ensp;

# Table des Matières

- [Guide d'Installation et de Configuration](#guide-dinstallation-et-de-configuration)
  - [Procédure de Configuration du Serveur Ubuntu 22.04 pour le Projet avec NVM et Node.js v18.16.1](#procédure-de-configuration-du-serveur-ubuntu-2204-pour-le-projet-avec-nvm-et-nodejs-v18161)
  - [Clonage du Dépôt Git](#clonage-du-dépôt-git)
  - [Installation des dépendances de Next.js](#installation-des-dépendances-de-nextjs)
  - [Installation d'Apache2](#installation-dapache2)
  - [Configuration d'Apache2 pour Strapi](#configuration-dapache2-pour-strapi)
  - [Configuration de PM2 pour Strapi](#configuration-de-pm2-pour-strapi)
  - [Configuration Next.js pour Apache2](#configuration-nextjs-pour-apache2)
  - [Configuration du Service Next.js](#configuration-du-service-nextjs)
  - [Exemple de Configuration Apache2 (`strapi.conf`)](#exemple-de-configuration-apache2-strapiconf)
  - [Exemple de Configuration Apache2 (`nextjs.conf`)](#exemple-de-configuration-apache2-nextjsconf)
  - [Procédure d’actualisation du contenu sur le serveur](#procédure-dactualisation-du-contenu-sur-le-serveur)
- [Fonctionnalités et Utilisation](#fonctionnalités-et-utilisation)
  - [Front-end](#front-end)
  - [Back-end](#back-end)
  - [Instructions pour les Administrateurs](#instructions-pour-les-administrateurs)
- [Gestion de Projet](#gestion-de-projet)
  - [Technologies Utilisées](#technologies-utilisées)
  - [Documentation Technique](#documentation-technique)
- [Précision sur certaines Variables et Paramètres modifiables](#précision-sur-certaines-variables-et-paramètres-modifiables)
  - [Page Liste des Podcasts](#page-liste-des-podcasts)
  - [Page Détail d’un Podcast [slug].tsx](#page-détail-dun-podcast-slugtsx)
  - [Composant Share](#composant-share)
  - [Composant AudioPlayer](#composant-audioplayer)
- [Remarques sur le projet](#remarques-sur-le-projet)
- [Améliorations possibles](#améliorations-possibles)

&ensp;

&ensp;


## Guide d'Installation et de Configuration

&ensp;

## Procédure de Configuration du Serveur Ubuntu 22.04 pour le Projet avec NVM et Node.js v18.16.1 :

1. **Installation de NVM (Node Version Manager)**:

   - Mettez à jour la liste des paquets : `sudo apt update`
   - Installez curl : `sudo apt install curl`
   - Téléchargez le script d'installation de NVM : `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash`
   - Redémarrez le terminal ou exécutez : `source ~/.bashrc`

2. **Installation de Node.js v18.16.1 avec NVM**:

   - Installez Node.js v18.16.1 : `nvm install v18.16.1`
   - Définissez la version par défaut : `nvm alias default v18.16.1`

3. **Clonage du Dépôt Git**:

   - Assurez-vous que Git est installé : `sudo apt install git`
   - Clonez votre dépôt Git : `git clone https://gitlab.com/jbcampan/projet-podcast-nextjs-strapi.git`

4. **Installation des dépendances de Next.js**:

   - Accédez au répertoire du projet front-end : `cd /projet-podcast-nextjs-strapi/client_nextjs`
   - Installez les dépendances : `npm install`
   - Compilez Next.js : `npm run build`

5. **Installation d'Apache2**:

   - Installez Apache2 : `sudo apt install apache2`

6. **Configuration d'Apache2 pour Strapi**:

   - Accédez au répertoire du projet back-end : `cd /projet-podcast-nextjs-strapi/server_strapi`
   - Installez les dépendances de Strapi : `npm install`
   - Créez un fichier de configuration pour Apache2 (ex : `strapi.conf`) dans `/etc/apache2/sites-available` pour Strapi
   - Activez la configuration : `sudo a2ensite strapi.conf`
   - Redémarrez Apache2 : `sudo systemctl restart apache2`

7. **Configuration de PM2 pour Strapi**:

   - Installez PM2 de manière globale : `sudo npm install -g pm2`
   - Démarrez Strapi avec PM2 depuis le répertoire de la partie Strapi : `pm2 start npm --name "server-strapi" -- start`

8. **Configuration Next.js pour Apache2**:

   - Créez un fichier de configuration pour Next.js (ex : `nextjs.conf`) dans `/etc/apache2/sites-available`
   - Activez la configuration : `sudo a2ensite nextjs.conf`
   - Redémarrez Apache2 : `sudo systemctl restart apache2`

9. **Configuration du Service Next.js**:

   - Créez un fichier de configuration de service pour Next.js (ex : `nextjs.service`) dans `/etc/systemd/system`
   - Activez le service : `sudo systemctl enable nextjs`
   - Démarrez le service Next.js : `sudo systemctl start nextjs`

&ensp;

&ensp;

### Exemple de Configuration Apache2 (`strapi.conf`)

Voici un exemple de fichier de configuration Apache2 pour Strapi. Ce fichier permet de rediriger les requêtes vers le serveur Strapi qui tourne en local.

```apache
<VirtualHost *:1337>
    ServerName nom.de.domaine (ou adresse IP)

    ProxyRequests Off
    ProxyPreserveHost On

    ProxyPass / http://127.0.0.1:1337/
    ProxyPassReverse / http://127.0.0.1:1337/

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```

&ensp;

### Exemple de Configuration Apache2 (`nextjs.conf`)

Voici un exemple de fichier de configuration Apache2 pour Next.js. Ce fichier permet de rediriger les requêtes vers le serveur Next.js qui tourne en local.

```apache
<VirtualHost *:80>
    ServerName nom.de.domaine (ou adresse IP)
    ServerAdmin email@mail.fr

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    ProxyPass / http://127.0.0.1:3000/
    ProxyPassReverse / http://127.0.0.1:3000/

    ProxyRequests Off
</VirtualHost>
``` 

&ensp;

&ensp;

## Procédure d’actualisation du contenu sur le serveur :

1. **Arrêter le Service Next.js en cours d'exécution** :
Arrêter le service Next.js : `sudo systemctl stop nextjs` 

2. **Arrêter les Processus PM2 (Strapi est le seul processus)** :
Arrêter tous les processus gérés par PM2 : `pm2 stop all`

3. **Récupérer les Dernières Modifications depuis le Dépôt Git** :
Récupérer les dernières modifications du dépôt Git : `git pull`

4. **Accéder au Répertoire du Projet Front-End** :
Naviguer vers le répertoire du projet front-end : `cd /client_nextjs`

5. **Reconstruire le Projet Front-End** :
Reconstruire le projet front-end : `npm run build`

6. **Redémarrer les Processus PM2 (donc uniquement Strapi)** :
Redémarrer les processus gérés par PM2 : `pm2 start all` 

7. **Redémarrer le Service Next.js** :
Redémarrer le service Next.js : `sudo systemctl start nextjs`

&ensp;

&ensp;

## Fonctionnalités et Utilisation

### Front-end

Voici les principales fonctionnalités mises en place :

- **Navigation :** La navigation entre les différentes pages du site est définie dans le composant Header, et donne accès via le composant Search à la recherche d’un podcast par titre.

- **Page d'Accueil :** La page d'accueil présente des sections distinctes, dont une bannière d'introduction, une description du site, une sélection de podcasts recommandés, une invitation à participer, un formulaire d’inscription à la newsletter.

- **Exploration des Podcasts :** La page Podcasts propose une liste de podcasts avec des options de tri et de recherche par date ou par nombre d’écoutes, permettant aux utilisateurs de découvrir et de filtrer les podcasts en fonction de leurs préférences. Un système de pagination est également intégré pour naviguer entre les différentes pages de liste des podcasts.

- **Détail du Podcast :** Les pages de détail des podcasts fournissent des informations complètes sur chaque épisode, y compris une description détaillée, un lecteur multimédia, une liste de moments associés, des options pour partager, commenter ou évaluer le podcast. En particulier, grâce à l'intégration de Disqus, les utilisateurs peuvent laisser des commentaires et des évaluations sur les podcasts, encourageant ainsi l'engagement de la communauté. Les fonctionnalités de partage sur les réseaux sociaux sont intégrées pour encourager l'interaction et la diffusion des podcasts. Une fonctionnalité d’incrémentation du nombre d’écoutes d’un podcast est intégrée pour permettre de mesurer son impact auprès de la communauté.

- **Formulaire de Contact :** La page "Contact" inclut un formulaire Typeform interactif qui permet aux utilisateurs de soumettre des sujets pour de nouveaux podcasts, de contacter l’équipe pour un partenariat, de demander à participer à un podcast, ou toute autre demande.

- **Compatibilité Mobile :** Le design réactif assure une expérience cohérente sur différents appareils, garantissant que les utilisateurs puissent profiter pleinement de toutes les fonctionnalités, qu'ils utilisent un ordinateur de bureau, une tablette ou un smartphone.

&ensp;

### Back-end

- **Gestion des Données (Strapi) :** Le back-end utilise Strapi, un système de gestion de contenu (CMS) open-source, pour stocker et gérer les données relatives aux podcasts, aux épisodes, aux catégories et aux utilisateurs. Les administrateurs peuvent ajouter, mettre à jour et supprimer des contenus via l'interface d'administration conviviale de Strapi.

- **Modèle de Données et Relations :** Le modèle de données est structuré pour représenter les entités clés telles que les podcasts, les épisodes, les catégories, les invités, les hôtes ou encore les horodatages. Des relations sont établies entre ces entités pour permettre une navigation fluide et une gestion cohérente des données.

- **Points d'Accès API :** Les requêtes permettent de récupérer précisément les données nécessaires depuis différents points d'accès API, améliorant les performances et minimisant la quantité de données récupérées.

&ensp;

### Instructions pour les Administrateurs :

- **Gestion des Commentaires (Disqus) :** Les administrateurs peuvent se connecter à la plateforme Disqus en utilisant les informations d'identification fournies. Ils peuvent modérer les commentaires, répondre aux utilisateurs et gérer l'interaction au niveau des commentaires sur les pages de détail des podcasts.

- **Gestion des Podcasts (Strapi) :** L'interface d'administration de Strapi permet aux administrateurs de gérer les podcasts, les catégories, les hôtes, les participants (invités), les horodatages, les informations de contact (si n’utilisent pas typeform). Ils peuvent ajouter de nouveaux podcasts, mettre à jour les détails, télécharger des médias.

&ensp;

&ensp;

## Gestion de Projet

### Technologies Utilisées

Voici la liste des principales technologies utilisées :

- **Next.js :** Framework de développement web basé sur React, favorisant le rendu côté serveur (SSR) et le rendu côté client (CSR), offrant ainsi une expérience utilisateur rapide et fluide.

-  **React :** Bibliothèque JavaScript pour la construction d'interfaces utilisateur interactives et réactives, formant la base de l'interface utilisateur du projet.

-  **TypeScript :** Langage de programmation qui apporte le typage statique au JavaScript, améliorant ainsi la maintenabilité, la lisibilité et la robustesse du code.

-  **Tailwind CSS :** Framework CSS facilitant la création d'interfaces utilisateur esthétiques en utilisant des classes prédéfinies, permettant un développement plus rapide et cohérent.

-  **Strapi :** CMS (Système de Gestion de Contenu) headless open source pour la création et la gestion de contenu, utilisé pour stocker et gérer les données des podcasts, des catégories, etc...

- **Disqus :** Plateforme de gestion de commentaires offrant une interaction utilisateur avancée, permettant aux utilisateurs de commenter et d'évaluer les podcasts.

-  **Typeform :** Plateforme de création de formulaires en ligne, intégrée pour fournir une expérience utilisateur fluide et interactive lors de la soumission de sujets pour les podcasts, ou tout autre demande.

-  **React Icons :** Bibliothèque de composants d'icônes prêts à l'emploi pour React, simplifiant l'ajout d'icônes personnalisées dans l'interface utilisateur.

-  **React-Share :** Bibliothèque open source permettant l'intégration facile de boutons de partage vers les réseaux sociaux, améliorant la diffusion et la visibilité des podcasts.

&ensp;

&ensp;


## Documentation Technique

Voici une explication de la structure du code source :

**Dossiers Principaux :**

#### `server_strapi`

Ce dossier contient tous les fichiers et configurations liés au backend de l'application, géré par Strapi : modèles de données, les types de contenu et les configurations de base de données, etc…

Seul 1 fichier a été personnalisé directement dans le code : `/src/api/podcast/controllers/podcast.js`

#### `client_nextjs`

Ce dossier englobe tous les aspects du frontend de l'application, développé en utilisant Next.js, et notamment les sous-dossiers :

- **`components` :** Ce dossier contient des composants réutilisables utilisés à travers l'application, tels que les barres de navigation, les cartes de podcast, etc.

- **`context` :** Ce dossier contient le contexte de l'API Strapi, permettant la récupération et le partage des données à travers l'application.

- **`pages` :** Les différentes pages de l'application sont organisées ici. Chaque page correspond à un fichier dans ce dossier et suit les conventions de routage de Next.js.

- **`public` :** Ce dossier contient des fichiers statiques tels que des images et des icônes, accessibles via des URL absolues.

- **`styles` :** Le fichier de styles globaux et personnalisés est stocké ici. Pour les styles, la majorité sont appliqués directement avec les classes Tailwind. D’autres fichiers de styles sont présents : il s’agit de fichiers de styles associés à chacun des composants.

&ensp;

### Gestion des Outils Tiers :

Les intégrations d'outils tiers tels que Disqus et Typeform sont faites via des balises et des scripts associés qui leur sont spécifiques.

&ensp;

### Description des modèles de données :

La base de données du projet repose sur Strapi. Celle par défaut fournie par Strapi a été conservée, utilisant SQLite comme système de gestion de base de données. Elle permet de stocker les informations relatives aux podcasts, aux catégories, aux hôtes, aux participants, aux horodatages.

&ensp;

### Remarques :

- La collection User par défaut n’a pas été modifiée.
- La collection Contact permet de stocker les informations du formulaire de contact (le premier réalisé avant intégration du Typeform).
- La collection commentaire avait été faite en prévision du stockage des commentaires (avant l’intégration de Disqus). Elle n’est pas fonctionnelle.

&ensp;

### Récupération de Données via les APIs :

Les données des podcasts sont récupérées via l'API de Strapi :

- Le contexte `apiContext.tsx` (dans `/context`) permet une partie de la récupération des données. Il partage ses informations avec la page d'Accueil, la de Liste des Podcasts et le composant de recherche ‘Search’.

- Un second appel à l'API est effectué dans la page de détail d'un podcast `[slug].tsx`. Lorsqu'un utilisateur accède à cette page via un URL unique (slug), cet appel récupère les informations détaillées du podcast associé au slug.

Dans Strapi, seul le fichier /src/api/podcast/controllers/podcast.js a été customisé, afin de récupérer les données des podcasts depuis l’API non plus en fonction de leur ID (comme cela se fait par défaut) mais en fonction de leur slug (configuré pour correspondre au titre du podcast).

&ensp;

&ensp;

## Précision sur certaines Variables et Paramètres modifiables :

Les balises `<meta>` des différentes pages peuvent être personnalisées pour optimiser les informations affichées sur les moteurs de recherche et les plateformes de partage. Vous avez également la possibilité d'utiliser la bibliothèque ‘next-seo’ en complément ou en remplacement pour gérer ces méta-informations de manière plus détaillée.


#### Page Liste des Podcasts :

- `podcastPerPage`: Cette variable permet de définir le nombre de podcasts affichés par page. Dans la mesure où les podcasts sont affichés dans 1, 2, 3 ou 4 colonnes en fonction des différentes tailles d’écran, je préconise de choisir un multiple de 2, 3 et 4 pour éviter qu’il n’y ait des trous lorsqu’il y a plusieurs pages de podcasts. Donc plutôt choisir 12, 24, 36, 48, etc.

- `maxDisplayedButtons`: Cette variable détermine le nombre maximum de boutons de navigation de page à afficher. Vous pouvez la personnaliser en fonction de vos préférences.


#### Page Détail d’un Podcast [slug].tsx :

- `link.target`: Cette variable permet de spécifier le nom des fichiers audio de podcasts téléchargeables. C’est là que vous pouvez le personnaliser.


#### Composant Share :

- `imageUrl`: Cette variable peut être modifiée pour personnaliser l'image associée au partage du podcast sur Pinterest via le composant de partage. (non testé).


#### Composant AudioPlayer :

- `backThirty`: Cette variable permet de définir le temps de recul lors de l'utilisation du bouton de retour en arrière du lecteur audio. Il est défini pour le moment à 30 secondes.

- `forwardThirty`: Cette variable permet de définir le temps d’avancement lors de l'utilisation du bouton de saut en avant du lecteur audio. Il est défini pour le moment à 30 secondes.

- `condition (if minutes === 0 && seconds === 10)`: Vous pouvez personnaliser le moment précis à partir duquel le nombre d'écoutes d'un podcast est incrémenté. Actuellement, on incrémente à partir de 10 secondes d’écoute cumulée (pratique pour la phase de test). Vous pouvez modifier cette condition pour adapter la manière dont les écoutes sont comptabilisées.

&ensp;

## Remarques sur le projet :

1. **Newsletter non fonctionnelle :** La partie fonctionnelle de la newsletter n'a pas été mise en place.

2. **Extension .js de /podcasts/index.js :** En raison d'un problème de typage, ce fichier est en .js et non .tsx, autrement il empêche la compilation sur le serveur.

3. **Mise en page de la Liste des Podcasts :** Lorsqu'un seul podcast est sélectionné via les catégories, la mise en page sur grand écran n’est pas optimale puisque le podcast prend toute la largeur de la page et devient disproportionné.


&ensp;

### Améliorations possibles :

1. **Composant Bouton :** Créer un composant de bouton réutilisable pour maintenir un code plus cohérent et faciliter l'ajout de boutons dans différentes parties de l'application.

2. **Composant Témoignages :** Créer un composant dédié aux témoignages pour améliorer la mise en forme du code.

3. **Nombre de Commentaires des podcasts :** Récupérer le nombre de commentaires pour chaque podcast à partir de Disqus et de l'afficher sur la page de liste des podcasts comme sur initialement prévu sur la maquette.

4. **Intégration de next-seo :** L'ajout de next-seo pourrait améliorer le référencement et l'apparence des pages partagées sur les réseaux sociaux en fournissant des balises méta personnalisées.

5. **Incrémentation du Nombre d'Écoutes :** Envisager d’incrémenter le nombre d'écoutes d'un podcast dès qu'un utilisateur clique sur le bouton de téléchargement, ce qui peut offrir une mesure plus précise de l'engagement de l'audience.

6. **Composant Pagination :** Créer un composant dédié au système de pagination afin d’améliorer l’organisation et la lisibilité du code.
