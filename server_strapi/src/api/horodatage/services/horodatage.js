'use strict';

/**
 * horodatage service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::horodatage.horodatage');
