'use strict';

/**
 * horodatage router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::horodatage.horodatage');
