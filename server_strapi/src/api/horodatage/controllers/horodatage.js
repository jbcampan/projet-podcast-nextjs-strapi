'use strict';

/**
 * horodatage controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::horodatage.horodatage');
