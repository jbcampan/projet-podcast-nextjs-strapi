'use strict';

/**
 * hote controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::hote.hote');
