'use strict';

/**
 * hote service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::hote.hote');
