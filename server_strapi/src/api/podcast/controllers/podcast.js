'use strict';

/**
 * podcast controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::podcast.podcast', ({ strapi }) =>  ({
    async findOne(ctx) {
        const { id } = ctx.params;

        const entity = await strapi.db.query('api::podcast.podcast').findOne({
            where: { slug: id },
            populate: ['categories','img','mp3','hotes','participants','horodatages','commentaires']
        });

        if (!entity) {
            return ctx.notFound();
        }

        const sanitizedEntity = await this.sanitizedOutput(entity, ctx);

        // Return the sanitized entity directly
        return sanitizedEntity;
    },

    async sanitizedOutput(entity, ctx) {
        // Define your logic for sanitizing the output entity here
        return entity;
    }
}));






